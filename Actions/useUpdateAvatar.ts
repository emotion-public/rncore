import uploadFile from 'Core/Api/Upload';
import { useUpdateCurrentUser } from 'Core/hooks/useCurrentUserActions';
import { LogEvent } from 'Core/Libs/Amplitude';
import { useTranslation } from 'react-i18next';
import { Alert } from 'react-native';

export default function useUpdateAvatar() {
  const { t } = useTranslation();
  const [updateMe] = useUpdateCurrentUser();

  return async (imageUri: string) => {
    try {
      LogEvent('Start Upload Avatar');
      let avatarUri = await uploadFile(imageUri);
      LogEvent('Upload Avatar Success');
      updateMe({
        avatar: avatarUri,
      });
    } catch (e: any) {
      LogEvent('Upload Avatar Error');
      Alert.alert(t('error_defaultMessage'));
    }
  };
}
