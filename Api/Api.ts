import axios from 'axios';
import CONFIG from 'Config';

let accessTokenGetter = () => null;
export const setApiAccessTokenGetter = (_accessTokenGetter: any) => {
  accessTokenGetter = _accessTokenGetter;
};
export const getAccessTokenGetter = () => {
  return accessTokenGetter;
};

export async function apiCall({
  path,
  method,
  data,
}: {
  path: string;
  method: 'POST' | 'GET';
  data: any;
}) {
  const rs = await axios({
    baseURL: CONFIG.base_api_url,
    url: path,
    method,

    data,
  });

  return rs.data;
}

export async function authPhone({ phone }: { phone: string }) {
  const auth = await apiCall({
    path: '/auth/phone/step1',
    method: 'GET',
    data: { phone },
  });

  if (auth.error) {
    throw new Error(auth.error.message || 'Sorry an error has occured !');
  }

  return auth.access_token;
}
