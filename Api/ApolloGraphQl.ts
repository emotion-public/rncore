import {
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  TypePolicies,
  from,
} from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { AsyncStorageWrapper, persistCache } from 'apollo3-cache-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CONFIG from 'Config';

let accessTokenGetter: any;
export function setAccessTokenGetter(fct: any) {
  accessTokenGetter = fct;
}
function getAccessTokenGetter() {
  return accessTokenGetter;
}

let sharedClient: ApolloClient<any>;
export function setApolloSharedClient(_sharedClient: ApolloClient<any>) {
  sharedClient = _sharedClient;
}

export function getApolloSharedClient() {
  return sharedClient;
}

export const createApolloClient = async (
  config: {
    access_token?: string;
    typePolicies?: TypePolicies;
    onUnauthenticatedError?: () => any;
  } = {},
) => {
  const { access_token, typePolicies, onUnauthenticatedError } = config;
  const authLink = setContext((_, { headers }) => {
    let token = null;
    if (access_token) {
      token = access_token;
    } else if (getAccessTokenGetter) {
      const getAccessToken = getAccessTokenGetter();
      token = getAccessToken();
    }
    const rqHeaders = {
      ...headers,
    };
    if (token) {
      rqHeaders.authorization = `Bearer ${token}`;
    }

    // console.log('Configure AuthLink Apollo with token ', token);
    return {
      headers: rqHeaders,
    };
  });

  const httpLink = createHttpLink({
    uri: CONFIG.graphql_url,
  });

  const cache = new InMemoryCache({
    typePolicies: typePolicies || {},
  });
  await persistCache({
    cache,
    storage: new AsyncStorageWrapper(AsyncStorage),
  });
  return new ApolloClient({
    link: from([
      onError(({ graphQLErrors }) => {
        if (graphQLErrors) {
          graphQLErrors.forEach(graphQLError => {
            if (
              onUnauthenticatedError &&
              graphQLError.extensions.code === 'UNAUTHENTICATED'
            ) {
              onUnauthenticatedError();
            }
          });
        }
      }),
      authLink,
      httpLink,
    ]),
    cache,
  });
};
