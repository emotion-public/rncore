import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { apiCall } from 'Core/Api/Api';

export async function authWithPhoneNumber(phoneNumber: string) {
  const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
  return async (code: string) => {
    return await confirmation.confirm(code);
  };
}

let confirmation: FirebaseAuthTypes.ConfirmationResult;
export default class AuthWithPhoneNumber {
  static async signInWithPhoneNumber(phoneNumber: string) {
    if (auth().currentUser) {
      await auth().signOut();
    }
    confirmation = await auth().signInWithPhoneNumber(phoneNumber);
  }

  static async confirmWithCode(code: string): Promise<string> {
    if (!confirmation) {
      throw new Error('An error has occured');
    }

    return new Promise(async (resolve, reject) => {
      try {
        const unsubscribe = auth().onAuthStateChanged(async user => {
          try {
            if (user) {
              const idToken = await auth().currentUser?.getIdToken();
              if (!idToken) {
                throw new Error('Unknown');
              }
              resolve(idToken);
              unsubscribe();
            }
          } catch (e) {
            reject(e);
          }
        });

        await confirmation.confirm(code);
      } catch (e) {
        reject(e);
      }
    });
  }

  static async authWithIdToken(idToken: string) {
    const rs = await apiCall({
      path: 'auth/firebase/withToken',
      method: 'POST',
      data: { idToken },
    });

    if (rs.error || !rs.access_token) {
      throw new Error(rs.message || 'Unknown error');
    }

    return rs.access_token;
  }
}
