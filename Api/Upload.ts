import CONFIG from 'Config';
import * as FileSystem from 'expo-file-system';

// @TODO Change to https://github.com/itinance/react-native-fs to have upload progress

export default async function uploadFile(
  uri: string,
  options?: {
    onProgress?: (progress: number) => any;
  },
) {
  const { onProgress = () => {} } = options || {};

  onProgress(1 / 100);

  const uploadResult = await FileSystem.uploadAsync(
    CONFIG.base_api_url + '/upload',
    uri,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      httpMethod: 'POST',
      uploadType: FileSystem.FileSystemUploadType.MULTIPART,
      fieldName: 'uploadfile',
    },
  );

  onProgress(1);

  try {
    const result = JSON.parse(uploadResult.body) as any;
    if (!result.filename) {
      throw new Error('Missing filename');
    }
    return result.filename;
  } catch (e: any) {
    return false;
  }
}
