import i18n from 'i18next';
import * as Localization from 'expo-localization';
import { initReactI18next } from 'react-i18next';
import { TypePolicies } from '@apollo/client';
import {
  createApolloClient,
  getApolloSharedClient,
  setAccessTokenGetter,
  setApolloSharedClient,
} from 'Core/Api/ApolloGraphQl';
import React, { useEffect, useRef, useState } from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ApolloProvider as ApolloProviderBase } from '@apollo/client/react';
import reduxStore, { RootState } from 'Store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useDevKeepAwake from 'Core/hooks/useDevKeepAwake';
import { ActionSheetProvider } from 'Core/Components/ActionSheet';
import { CONFIG, setCoreConfig } from 'Core/config';
import { initAmplitude, LogEvent } from 'Core/Libs/Amplitude';
import useOneSignalHandler from 'Core/Libs/OneSignal/useOneSignalHandler';
import initOneSignal from 'Core/Libs/OneSignal/init';
import useAppStateChanged from 'Core/hooks/useAppState';
import useSessionLogger from 'Core/hooks/useSessionLogger';
import { useAuth } from 'Core/hooks/useAuth';
import { useAuthActions } from 'Core/hooks/useAuthActions';
import delay from 'delay';

const { store, persistor } = reduxStore;

export type ApolloClientConfig = {
  apolloClientTypePolicies?: TypePolicies;
  onUnauthenticatedError?: () => any;
};
export type APP_CONFIG = {
  apolloClientConfig: ApolloClientConfig;
};

export async function initApolloClient(config?: ApolloClientConfig) {
  setAccessTokenGetter(() => {
    const storeState = store.getState() as RootState;
    return storeState.auth?.access_token || null;
  });
  setApolloSharedClient(
    await createApolloClient({
      typePolicies: config?.apolloClientTypePolicies || {},
      onUnauthenticatedError: config?.onUnauthenticatedError,
    }),
  );
}

export function initCore(config: CONFIG) {
  setCoreConfig(config);
  if (config.onesignal?.appId) {
    initOneSignal(config.onesignal.appId);
  }
  if (config.amplitude) {
    if (__DEV__) {
      initAmplitude(config.amplitude.apiKeyDev);
    } else {
      initAmplitude(config.amplitude.apiKeyProd);
    }
  }
}

export function initI18N({ Trads }: { Trads: any }) {
  i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
      resources: Trads,
      lng: Localization.locale,
      fallbackLng: 'en',

      interpolation: {
        escapeValue: false,
      },
    });
}

export function ProvidersInsideNavigation({
  children,
}: {
  children: React.ReactNode;
}) {
  return <ActionSheetProvider>{children}</ActionSheetProvider>;
}

function AppCoreWithContext({ children }: { children?: React.ReactNode }) {
  const authData = useAuth();
  const { fetchAllUserData } = useAuthActions();

  const { isLogged, access_token } = authData;

  useEffect(() => {
    if (isLogged) {
      fetchAllUserData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLogged, access_token]);

  return <>{children}</>;
}

function ApolloProvider({
  children,
  apolloClientConfig,
}: {
  children: React.ReactNode;
  apolloClientConfig: ApolloClientConfig;
}) {
  const [isApolloReady, setApolloReady] = useState(false);
  const { logout } = useAuthActions();
  const isLoggingOut = useRef(false);
  useEffect(() => {
    initApolloClient({
      ...apolloClientConfig,
      onUnauthenticatedError: async () => {
        if (!isLoggingOut.current) {
          console.log('TRYING TO LOGOUT 1111');
          isLoggingOut.current = true;
          logout();
          await delay(2000);
          isLoggingOut.current = false;
        }
        if (apolloClientConfig.onUnauthenticatedError) {
          apolloClientConfig.onUnauthenticatedError();
        }
      },
    }).then(async () => {
      setApolloReady(true);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!isApolloReady) {
    return null;
  }
  return (
    <ApolloProviderBase client={getApolloSharedClient()}>
      {children}
    </ApolloProviderBase>
  );
}

export default function AppCore({
  config,
  children,
}: {
  config: APP_CONFIG;
  children?: React.ReactNode;
}) {
  useDevKeepAwake();
  useOneSignalHandler();
  useSessionLogger();

  useEffect(() => {
    LogEvent('Launch App');
  }, []);

  useAppStateChanged(({ appState }) => {
    if (appState === 'active') {
      LogEvent('Open App');
    }

    if (appState === 'background') {
      LogEvent('App in Background');
    }
  });

  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ApolloProvider apolloClientConfig={config.apolloClientConfig}>
            <AppCoreWithContext>{children}</AppCoreWithContext>
          </ApolloProvider>
        </PersistGate>
      </Provider>
    </SafeAreaProvider>
  );
}
