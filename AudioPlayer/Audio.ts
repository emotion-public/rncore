import { Audio as ExpoAudio, InterruptionModeIOS } from 'expo-av';
import MusicControl, { Command } from 'react-native-music-control';
import * as FileSystem from 'expo-file-system';
import EventEmitter from 'eventemitter3';

export type SOUND_STATE = {
  isPlaying: boolean;
  isLoading: boolean;
  positionMillis: number;
  durationMillis: number;
};
export type SOUND_POSITION_STATE = {
  positionMillis: number;
};
type SOUND_EVENTS =
  | 'loading'
  | 'ready'
  | 'playing'
  | 'paused'
  | 'ended'
  | 'state_changed'
  | 'position_changed';
export class Audio {
  private id: string;
  private sound: ExpoAudio.Sound | null = null;
  private destroyed: boolean = false;
  private eventEmitter: EventEmitter<SOUND_EVENTS>;

  /// Internal State
  private _didJustFinish: boolean = false;
  private _isPlaying: boolean = false;
  private _isLoading: boolean = false;
  private _positionMillis: number = 0;
  private _durationMillis: number = 0;

  private _enableIsPlayingStateChange = true;
  private _enablePositionMillisStateChange = true;

  constructor(id: string) {
    this.id = id;
    this.eventEmitter = new EventEmitter<SOUND_EVENTS>();
  }

  isPlaying() {
    return this._isPlaying;
  }

  /// EVENTS
  on(eventName: SOUND_EVENTS, cb: (arg: any) => any) {
    this.eventEmitter.on(eventName, cb);
    return () => {
      this.eventEmitter.off(eventName, cb);
    };
  }
  onStateChange(eventName: SOUND_EVENTS, cb: (state: SOUND_STATE) => any) {
    this.eventEmitter.on(eventName, cb);
    return () => {
      this.eventEmitter.off(eventName, cb);
    };
  }
  getSoundState(): SOUND_STATE {
    return {
      isPlaying: this._isPlaying,
      isLoading: this._isLoading,
      positionMillis: this._positionMillis,
      durationMillis: this._durationMillis,
    };
  }

  getSoundPositionState(): SOUND_POSITION_STATE {
    return {
      positionMillis: this._positionMillis,
    };
  }
  emitStateChanged() {
    this.eventEmitter.emit('state_changed', this.getSoundState());
  }

  removeAllEventListeners() {
    this.eventEmitter.removeAllListeners();
  }
  /// EVENTS

  async destroy() {
    this.destroyed = true;
    MusicControl.off(Command.play);
    MusicControl.off(Command.pause);
    this.eventEmitter.removeAllListeners();
    if (!this.sound) {
      return;
    }
    await this.sound.unloadAsync();
  }

  async createSound() {
    this.sound = new ExpoAudio.Sound();

    MusicControl.enableControl('play', true);
    MusicControl.enableControl('pause', true);
    MusicControl.enableControl('stop', false);
    MusicControl.enableControl('changePlaybackPosition', true);

    MusicControl.enableControl('seekForward', true);

    MusicControl.on(Command.play, () => {
      this.resume();
    });
    MusicControl.on(Command.pause, () => {
      this.pause();
    });

    MusicControl.on(Command.changePlaybackPosition, playbackPosition => {
      this.seekTo(playbackPosition * 1000);
    });
  }

  async pause() {
    if (this.destroyed || !this.sound) {
      return false;
    }
    this._isPlaying = false;
    this.emitStateChanged();
    this._enableIsPlayingStateChange = false;
    await this.sound.pauseAsync();
    this._enableIsPlayingStateChange = true;
  }

  async play(url: string) {
    await this.createSound();
    if (!this.sound) {
      return false;
    }

    this._isLoading = true;
    this.emitStateChanged();
    await ExpoAudio.setAudioModeAsync({
      playThroughEarpieceAndroid: true,
      playsInSilentModeIOS: true,
      staysActiveInBackground: true,
      allowsRecordingIOS: false,
      interruptionModeIOS: InterruptionModeIOS.DoNotMix,
    });

    if (this.destroyed) {
      return false;
    }

    let playerUrl = url;
    const urlObj = new URL(url);
    let pathname = urlObj.pathname;
    if (pathname[0] === '/') {
      pathname = pathname.substring(1);
      pathname = pathname.replace(/\//g, '-');
    }
    const filePathCache = FileSystem.cacheDirectory + pathname;

    const fileCacheInfo = await FileSystem.getInfoAsync(filePathCache);

    if (fileCacheInfo.exists) {
      playerUrl = filePathCache;
    } else {
      FileSystem.downloadAsync(url, filePathCache);
    }

    if (this.destroyed) {
      return false;
    }

    const loadResult = await this.sound.loadAsync(
      {
        uri: playerUrl,
      },
      undefined,
      false,
    );

    if (this.destroyed) {
      return false;
    }

    // @ts-ignore
    this._durationMillis = loadResult.durationMillis;
    await this.sound.setProgressUpdateIntervalAsync(10);

    if (this.destroyed) {
      return false;
    }

    this.sound.setOnPlaybackStatusUpdate(playbackStatus => {
      if (this.destroyed) {
        return false;
      }
      let emitStateChanged = false;
      // @ts-ignore
      if (playbackStatus.didJustFinish) {
        this._didJustFinish = true;
        emitStateChanged = true;
      }

      // @ts-ignore
      let newDurationMillis = playbackStatus.durationMillis as number;
      // @ts-ignore
      let isPlaying = playbackStatus.isPlaying as boolean;
      if (newDurationMillis !== this._durationMillis) {
        this._durationMillis = newDurationMillis;
        emitStateChanged = true;
      }
      if (this._enableIsPlayingStateChange && isPlaying !== this._isPlaying) {
        this._isPlaying = isPlaying;
        emitStateChanged = true;
      }

      if (emitStateChanged) {
        this.emitStateChanged();
        if (this._didJustFinish) {
          this.eventEmitter.emit('ended');
        }
      }

      if (this._enablePositionMillisStateChange) {
        // @ts-ignore
        this._positionMillis = playbackStatus.positionMillis;
        if (this._durationMillis && this._positionMillis) {
          MusicControl.updatePlayback({
            duration: Math.floor(this._durationMillis / 1000),
            elapsedTime: Math.floor(this._positionMillis / 1000),
          });
        }
        this.eventEmitter.emit('position_changed');
      }
    });

    this._isLoading = false;
    this._isPlaying = true;
    this.eventEmitter.emit('ready');
    this.emitStateChanged();
    if (this._durationMillis === this._positionMillis) {
      await this.sound.replayAsync();
      return;
    }
    await this.sound.playAsync();
  }

  async resume() {
    if (this.destroyed || !this.sound) {
      return false;
    }

    this._isPlaying = true;
    this.emitStateChanged();
    if (this._didJustFinish || this._durationMillis === this._positionMillis) {
      this._didJustFinish = false;
      await this.sound.replayAsync();
      return;
    }
    await this.sound.playAsync();
  }

  async seekTo(positionMillis: number) {
    if (this.destroyed || !this.sound) {
      return false;
    }

    this._enablePositionMillisStateChange = false;
    const setPositionResult = await this.sound.setPositionAsync(positionMillis);
    this._enablePositionMillisStateChange = true;
    return setPositionResult;
  }
}
