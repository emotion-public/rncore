import EventEmitter from 'eventemitter3';
import { useCallback, useEffect, useState } from 'react';
import MusicControl from 'react-native-music-control';
import { Audio } from './Audio';

export type TRACK = {
  soundId: string;
  url: string;
  title: string;
  artwork?: string;
  payload?: any;
};
type PLAYER_STATE = {
  isPlaying: boolean;
  isLoading: boolean;
  durationMillis: number;
  soundId: string | null;
  currentTrack: TRACK | null;
};
type PLAYER_POSITION_STATE = {
  positionMillis: number;
};

type PLAYER_EVENTS =
  | 'play'
  | 'pause'
  | 'ended'
  | 'position_changed'
  | 'track_changed'
  | 'state_changed'
  | 'track_read';
type PLAYER_CONTEXT = { [key: string]: any };
export class Player {
  private track: TRACK | null = null;
  private sound: Audio | null = null;
  private eventEmitter: EventEmitter<PLAYER_EVENTS>;

  /// Internal State
  private _soundId: string | null = null;
  private _context: PLAYER_CONTEXT = {};

  constructor() {
    this.eventEmitter = new EventEmitter<PLAYER_EVENTS, any>();
  }

  /// EVENTS
  on(eventName: PLAYER_EVENTS, cb: () => any) {
    this.eventEmitter.on(eventName, cb);
    return () => {
      this.eventEmitter.off(eventName, cb);
    };
  }
  onTrackEnded(cb: (track: TRACK) => any) {
    this.eventEmitter.on('ended', cb);
    return () => {
      this.eventEmitter.off('ended', cb);
    };
  }
  onTrackRead(cb: (track: TRACK) => any) {
    this.eventEmitter.on('track_read', cb);
    return () => {
      this.eventEmitter.off('track_read', cb);
    };
  }
  /// EVENTS

  getPlayerState(): PLAYER_STATE {
    const { isLoading, isPlaying, durationMillis } =
      this.sound?.getSoundState() || {
        isLoading: false,
        isPlaying: false,
        durationMillis: 0,
      };
    return {
      isLoading,
      isPlaying,
      durationMillis,
      soundId: this._soundId,
      currentTrack: this.track,
    };
  }

  getPlayerPositionState(): PLAYER_POSITION_STATE {
    const { positionMillis } = this.sound?.getSoundState() || {
      positionMillis: 0,
    };
    return {
      positionMillis,
    };
  }

  getCurrentTrack() {
    return this.track;
  }

  getPlayerContext(): PLAYER_CONTEXT {
    return { ...this._context };
  }

  async seekTo(positionMillis: number) {
    if (!this.sound) {
      return false;
    }

    return await this.sound.seekTo(positionMillis);
  }

  async play(track: TRACK, context?: PLAYER_CONTEXT) {
    if (this.sound) {
      if (track.soundId && this._soundId === track.soundId) {
        return await this.resume();
      }
      this.stop();
      this.eventEmitter.emit('track_changed');
    }
    const { soundId } = track;
    this._soundId = soundId || null;
    this.track = track;
    this._context = context || {};

    MusicControl.enableBackgroundMode(true);
    MusicControl.setNowPlaying({
      title: 'Voicebox',
      artwork: this.track.artwork || '',
      artist: this.track.title || '',
    });
    this.sound = new Audio(soundId);
    this.sound.onStateChange('ready', () => {
      if (!this.track || !this.sound) {
        return false;
      }
      const soundState = this.sound.getSoundState();
      MusicControl.setNowPlaying({
        title: 'Voicebox',
        artwork: this.track.artwork || '',
        artist: this.track.title || '',
        duration: Math.floor(soundState.durationMillis / 1000),
      });
    });
    let trackRead = false;

    this.sound.onStateChange('position_changed', () => {
      const { positionMillis = 0, durationMillis = 0 } =
        this.sound?.getSoundState() || {};
      const alreadyRead = !!this.track?.payload?.read_at;
      if (
        !alreadyRead &&
        !trackRead &&
        durationMillis !== 0 &&
        ((durationMillis <= 3000 && positionMillis > durationMillis / 2) ||
          (durationMillis > 3000 && positionMillis >= 3000))
      ) {
        trackRead = true;
        this.eventEmitter.emit('track_read', this.getCurrentTrack());
      }

      this.eventEmitter.emit('position_changed');
    });
    this.sound.onStateChange('state_changed', () => {
      this.eventEmitter.emit('state_changed');
    });
    this.sound.on('ended', () => {
      this.eventEmitter.emit('ended', { ...this.track });
    });
    this.sound.play(this.track.url);
  }

  async resume() {
    if (!this.sound) {
      return false;
    }

    this.sound.resume();
  }

  async pause() {
    if (!this.sound) {
      return false;
    }

    this.sound.pause();
  }

  isPlaying() {
    return !this.sound || this.sound.isPlaying();
  }

  async playPause() {
    if (!this.sound) {
      return false;
    }
    if (this.sound.isPlaying()) {
      this.sound.pause();
    } else {
      this.sound.resume();
    }
  }

  async stop() {
    if (!this.sound) {
      return false;
    }
    this.sound.removeAllEventListeners();
    this.sound.pause();
    this.sound.destroy();
    this.sound = null;
    this._soundId = null;
    this.eventEmitter.emit('position_changed');
    this.eventEmitter.emit('state_changed');
  }
}

const player = new Player();

export function getSharedPlayer() {
  return player;
}

export function usePlayerState(soundId?: string) {
  const [state, setState] = useState<PLAYER_STATE>(player.getPlayerState());

  useEffect(() => {
    return player.on('state_changed', () => {
      setState(player.getPlayerState());
    });
  }, []);

  return {
    ...state,
    ...(!!soundId && soundId !== state.soundId
      ? {
          isPlaying: false,
          isLoading: false,
        }
      : {}),
  };
}

export function usePlayerPositionState() {
  const [state, setState] = useState<PLAYER_POSITION_STATE>(
    player.getPlayerPositionState(),
  );

  useEffect(() => {
    return player.on('position_changed', () => {
      setState(player.getPlayerPositionState());
    });
  }, []);

  return state;
}

export function usePlayerCurrentTrack() {
  const [currentTrack, setCurrentTrack] = useState<TRACK | null>(
    player.getCurrentTrack(),
  );

  // console.log('currentTrack la', currentTrack);

  useEffect(() => {
    return player.on('state_changed', () => {
      const newCurrentTrack = player.getCurrentTrack();

      if (
        !newCurrentTrack ||
        !currentTrack ||
        currentTrack.soundId !== newCurrentTrack.soundId
      ) {
        setCurrentTrack(newCurrentTrack);
      }
    });
  }, [currentTrack]);

  return currentTrack;
}

export function usePlayer() {
  return player;
}

export function usePlayerActions() {
  const playPause = useCallback(() => {
    player.playPause();
  }, []);
  return {
    playPause,
  };
}
