import { getSharedPlayer, Player, TRACK } from 'Core/AudioPlayer/Player';
import EventEmitter from 'eventemitter3';
import MusicControl, { Command } from 'react-native-music-control';
import { v4 as uuidv4 } from 'uuid';

type PLAYLIST_EVENTS = 'track_changed' | 'playlist_ended';
export default class Playlist {
  private uuid: string;
  private tracks: Array<TRACK>;
  private currentIndex: number;
  private playlistEnded: boolean = false;
  private eventEmitter: EventEmitter<PLAYLIST_EVENTS>;
  private player: Player;

  private _canBeDestroyed: boolean = false;
  private _trackEndedSubscribed: boolean = false;

  constructor(tracks: Array<TRACK>, player?: Player) {
    this.uuid = uuidv4();
    this.currentIndex = -1;
    this.tracks = tracks;
    this.eventEmitter = new EventEmitter<PLAYLIST_EVENTS>();
    this.player = player || getSharedPlayer();
  }

  canBeDestroyed() {
    return this._canBeDestroyed;
  }
  destroyWhenPossible() {
    this._canBeDestroyed = true;
  }
  destroy() {
    this.tracks = [];
    this.eventEmitter.removeAllListeners();
  }

  /// EVENTS
  onPlaylistEnded(cb: () => any) {
    this.eventEmitter.on('playlist_ended', cb);
    return () => {
      this.eventEmitter.off('playlist_ended', cb);
    };
  }
  /// EVENTS

  subscribeToTrackEndedEvent() {
    if (this._trackEndedSubscribed) {
      return;
    }
    this.player.onTrackEnded(this.onTrackEnded);
    this._trackEndedSubscribed = true;
  }

  getLastTrack() {
    if (this.tracks.length === 0) {
      return null;
    }
    return this.tracks[this.tracks.length - 1];
  }

  onTrackEnded = (track: TRACK) => {
    if (track.soundId !== this.getCurrentTrack()?.soundId) {
      return false;
    }
    const lastTrack = this.getLastTrack();
    if (lastTrack?.soundId === track.soundId) {
      this.eventEmitter.emit('playlist_ended');
      this.playlistEnded = true;
      return false;
    }
    this.next();
  };

  setTracks(tracks: Array<TRACK>) {
    this.tracks = tracks;
  }
  getTracks() {
    return [...this.tracks];
  }

  getUUID() {
    return this.uuid;
  }

  getCurrentTrack(): TRACK | null {
    if (this.currentIndex === -1) {
      return null;
    }

    return this.tracks[this.currentIndex];
  }

  isPlayingTrack(track: TRACK) {
    if (track.soundId !== this.getCurrentTrack()?.soundId) {
      return false;
    }
    const playerState = this.player.getPlayerState();
    if (!playerState.isPlaying || playerState.soundId !== track.soundId) {
      return false;
    }
    return true;
  }

  async playTrack(trackToPlay: TRACK) {
    const trackToPlayIndex = this.tracks.findIndex(
      trackInList => trackInList.soundId === trackToPlay.soundId,
    );
    if (trackToPlayIndex === -1) {
      return false;
    }
    if (this.currentIndex !== trackToPlayIndex) {
      this.currentIndex = trackToPlayIndex;
    }

    if (this.player.getPlayerContext().playlistUUID !== this.uuid) {
      // The context has changed => It means that something else has taken control of the player
      MusicControl.off(Command.nextTrack);
      MusicControl.off(Command.previousTrack);
      MusicControl.enableControl('nextTrack', true);
      MusicControl.enableControl('previousTrack', true);

      MusicControl.on(Command.nextTrack, () => {
        this.next();
      });
      MusicControl.on(Command.previousTrack, () => {
        this.prev();
      });
    }

    this.subscribeToTrackEndedEvent();

    return await this.player.play(this.tracks[this.currentIndex], {
      playlistUUID: this.uuid,
    });
  }

  async pauseTrack(track: TRACK) {
    if (!this.isPlayingTrack(track)) {
      return false;
    }
    return this.player.pause();
  }

  async play() {
    if (this.tracks.length === 0) {
      return false;
    }

    if (this.currentIndex === -1) {
      this.currentIndex = 0;
    }

    if (this.playlistEnded) {
      this.playlistEnded = false;
      this.currentIndex = 0;
    }

    return await this.playTrack(this.tracks[this.currentIndex]);
  }

  async next() {
    if (this.currentIndex === -1) {
      return false;
    }
    let nextIndex = this.currentIndex + 1;
    if (nextIndex > this.tracks.length - 1) {
      nextIndex = 0;
    }
    return await this.playTrack(this.tracks[nextIndex]);
  }
  async prev() {
    if (this.currentIndex === -1) {
      return false;
    }
    let nextIndex = this.currentIndex - 1;
    if (nextIndex < 0 || nextIndex > this.tracks.length - 1) {
      nextIndex = this.tracks.length - 1;
    }
    return await this.playTrack(this.tracks[nextIndex]);
  }
}
