import { Audio as ExpoAudio, InterruptionModeIOS } from 'expo-av';
import EventEmitter from 'eventemitter3';

export type SOUND_STATE = {
  isPlaying: boolean;
  isLoading: boolean;
  positionMillis: number;
  durationMillis: number;
};
type SOUND_EVENTS = 'loading' | 'playing' | 'paused' | 'state_changed';
export class RecordedAudio {
  private localUri: string;
  private sound: ExpoAudio.Sound | null = null;
  private destroyed: boolean = false;
  private eventEmitter: EventEmitter<SOUND_EVENTS>;

  /// Internal State
  private _isPlaying: boolean = false;
  private _isLoading: boolean = false;
  private _positionMillis: number = 0;
  private _durationMillis: number = 0;

  constructor(localUri: string) {
    this.localUri = localUri;
    this.eventEmitter = new EventEmitter<SOUND_EVENTS>();
  }

  /// EVENTS
  onStateChange(eventName: SOUND_EVENTS, cb: (state: SOUND_STATE) => any) {
    this.eventEmitter.on(eventName, cb);
    return () => {
      this.eventEmitter.off(eventName, cb);
    };
  }
  getSoundState(): SOUND_STATE {
    return {
      isPlaying: this._isPlaying,
      isLoading: this._isLoading,
      positionMillis: this._positionMillis,
      durationMillis: this._durationMillis,
    };
  }
  emitStateChanged() {
    this.eventEmitter.emit('state_changed', this.getSoundState());
  }
  /// EVENTS

  async destroy() {
    this.destroyed = true;
    this.eventEmitter.removeAllListeners();
    if (!this.sound) {
      return;
    }
    await this.sound.unloadAsync();
  }

  async load() {
    this.sound = new ExpoAudio.Sound();
    this._isLoading = true;
    this.emitStateChanged();

    if (this.destroyed) {
      return false;
    }

    const loadResult = await this.sound.loadAsync({
      uri: this.localUri,
    });

    // @ts-ignore
    this._durationMillis = loadResult.durationMillis;
    await this.sound.setProgressUpdateIntervalAsync(10);
    this.sound.setOnPlaybackStatusUpdate(playbackStatus => {
      // @ts-ignore
      this._positionMillis = playbackStatus.positionMillis;
      // @ts-ignore
      this._durationMillis = playbackStatus.durationMillis;
      // @ts-ignore
      this._isPlaying = playbackStatus.isPlaying;
      this.emitStateChanged();
    });

    this._isLoading = false;
    this.emitStateChanged();
  }

  async seekTo(positionMillis: number) {
    if (this.destroyed || !this.sound) {
      return false;
    }

    return await this.sound.setPositionAsync(positionMillis);
  }

  async pause() {
    if (this.destroyed || !this.sound) {
      return false;
    }
    this._isPlaying = false;
    this.emitStateChanged();
    await this.sound.pauseAsync();
  }

  async play() {
    if (this.destroyed || !this.sound) {
      return false;
    }

    await ExpoAudio.setAudioModeAsync({
      playThroughEarpieceAndroid: true,
      playsInSilentModeIOS: true,
      staysActiveInBackground: true,
      allowsRecordingIOS: false,
      interruptionModeIOS: InterruptionModeIOS.DoNotMix,
    });

    if (this.destroyed) {
      return false;
    }

    this._isPlaying = true;
    this.emitStateChanged();
    if (this._durationMillis === this._positionMillis) {
      await this.sound.replayAsync();
      return;
    }
    await this.sound.playAsync();
  }
}
