import { useEffect, useRef } from 'react';
import Playlist from 'Core/AudioPlayer/Playlist';
import { TRACK } from 'Core/AudioPlayer/Player';
import { useGlobalPlaylist } from 'Components/Player/CurrentPlaylist';

export default function usePlaylist(
  tracks: Array<TRACK>,
  autorefresh: boolean = true,
  debug: boolean = false,
): [playlist: React.MutableRefObject<Playlist>, refreshPlaylist: () => any] {
  const playlist = useRef<Playlist>(new Playlist(tracks));
  const [currentGlobalPlaylist] = useGlobalPlaylist();
  const currentGlobalPlaylistUUID = currentGlobalPlaylist?.getUUID();

  useEffect(() => {
    if (autorefresh) {
      if (debug) {
        console.log('SET TRACKS !');
      }
      playlist.current.setTracks(tracks);
    }
  }, [tracks, autorefresh, debug]);

  useEffect(() => {
    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      const playlistObj = playlist.current;
      if (!playlistObj) {
        return;
      }
      playlistObj.destroyWhenPossible();
    };
  }, []);

  useEffect(() => {
    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      const playlistObj = playlist.current;
      if (currentGlobalPlaylistUUID !== playlistObj.getUUID()) {
        playlistObj.destroy();
      }
    };
  }, [currentGlobalPlaylistUUID]);

  return [
    playlist,
    function refresh() {
      playlist.current.setTracks(tracks);
    },
  ];
}
