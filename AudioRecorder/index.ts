import { Audio, AudioMode } from 'expo-av';
import EventEmitter from 'eventemitter3';
import { Alert, Linking } from 'react-native';

const RECORDING_OPTIONS = {
  isMeteringEnabled: true,
  android: {
    extension: '.m4a',
    outputFormat: Audio.RECORDING_OPTION_ANDROID_OUTPUT_FORMAT_AAC_ADTS,
    audioEncoder: Audio.RECORDING_OPTION_ANDROID_AUDIO_ENCODER_AAC_ELD,
    sampleRate: 44100,
    numberOfChannels: 2,
    bitRate: 128000,
  },
  ios: {
    extension: '.m4a',
    outputFormat: Audio.RECORDING_OPTION_IOS_OUTPUT_FORMAT_MPEG4AAC,
    audioQuality: Audio.RECORDING_OPTION_IOS_AUDIO_QUALITY_MIN,
    sampleRate: 44100,
    numberOfChannels: 1,
    bitRateStrategy: Audio.RECORDING_OPTION_IOS_BIT_RATE_STRATEGY_CONSTANT,
    linearPCMBitDepth: 16,
    linearPCMIsBigEndian: false,
    linearPCMIsFloat: false,
    bitRate: 64000,
  },
  web: {
    mimeType: 'audio/webm',
    bitsPerSecond: 128000,
  },
};

export type RecordingStatus = Audio.RecordingStatus;

export class Recorder {
  private hasPermission: boolean = false;
  private recording: boolean = false;
  private recordingDurationMillis: number = 0;
  private recordingInstance: Audio.Recording | null = null;
  private eventEmitter: EventEmitter<'update'>;

  constructor() {
    this.eventEmitter = new EventEmitter();
  }

  onRecordUpdate(cb: (status: Audio.RecordingStatus) => any) {
    this.eventEmitter.on('update', cb);
    return () => {
      this.eventEmitter.off('update', cb);
    };
  }

  public isRecording() {
    return this.recording;
  }

  public async requestPermissionIfNecessary(): Promise<{
    hasPermission: boolean;
    permissionHasBeenAsked: boolean;
  }> {
    if (this.hasPermission) {
      return { hasPermission: true, permissionHasBeenAsked: false };
    }

    let permissionHasBeenAsked = false;
    if (!this.hasPermission) {
      const permissions = await Audio.getPermissionsAsync();
      if (!permissions.granted) {
        if (!permissions.canAskAgain) {
          Alert.alert(
            'Authorize permissions from settings',
            'Go to settings and authorize microphone please',
            [
              {
                text: 'Cancel',
                onPress: () => {},
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => {
                  Linking.openSettings();
                },
              },
            ],
          );
          return { hasPermission: false, permissionHasBeenAsked: false };
        }
        permissionHasBeenAsked = true;
        const requestedPermissions = await Audio.requestPermissionsAsync();
        if (!requestedPermissions.granted) {
          return { hasPermission: false, permissionHasBeenAsked };
        }
      }

      this.hasPermission = true;
    }

    return { hasPermission: this.hasPermission, permissionHasBeenAsked };
  }

  public async startRecorder(options?: {
    onRecordingStatusUpdate?: (status: Audio.RecordingStatus) => any;
    maximumTimeSeconds?: number;
    onStopped?: (recordingResult: RecordingResult | false) => any;
  }) {
    if (this.recording) {
      return false;
    }

    this.recordingDurationMillis = 0;

    const {
      onStopped = (_: RecordingResult | false) => {},
      onRecordingStatusUpdate,
      maximumTimeSeconds,
    } = options || {};

    if (!this.hasPermission) {
      return false;
    }

    const audioMode: Partial<AudioMode> = {
      allowsRecordingIOS: true,
      playsInSilentModeIOS: true,
      staysActiveInBackground: true,
    };
    await Audio.setAudioModeAsync(audioMode);

    this.recording = true;
    this.recordingInstance = new Audio.Recording();
    let stopped = false;
    this.recordingInstance.setOnRecordingStatusUpdate(
      (status: Audio.RecordingStatus) => {
        if (status.durationMillis) {
          this.recordingDurationMillis = status.durationMillis;
        }
        this.eventEmitter.emit('update', status);
        if (onRecordingStatusUpdate) {
          onRecordingStatusUpdate(status);
        }

        if (
          maximumTimeSeconds !== undefined &&
          this.recordingDurationMillis >= maximumTimeSeconds * 1000
        ) {
          if (!stopped) {
            stopped = true;
            setTimeout(async () => {
              onStopped(await this.stopRecording());
            }, 0);
          }
        }
      },
    );
    this.recordingInstance.setProgressUpdateInterval(10);
    try {
      await this.recordingInstance.prepareToRecordAsync(RECORDING_OPTIONS);
      await this.recordingInstance.startAsync();
    } catch (e: any) {
      console.log(e.message);
    }
    return true;
  }

  stopRecording = async (): Promise<RecordingResult | false> => {
    if (!this.recording || !this.recordingInstance) {
      return false;
    }
    try {
      await this.recordingInstance.stopAndUnloadAsync();
    } catch (e: any) {}
    const uri = this.recordingInstance.getURI();

    const durationMillis = this.recordingDurationMillis;

    this.recordingDurationMillis = 0;
    this.recordingInstance = null;
    this.recording = false;

    if (!uri) {
      return false;
    }

    return {
      uri,
      durationMillis,
    };
  };
}

export type RecordingResult = {
  uri: string;
  durationMillis: number;
};

const sharedRecorder = new Recorder();
export default sharedRecorder;
