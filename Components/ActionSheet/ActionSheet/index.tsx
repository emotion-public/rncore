import LargeButton, {
  LargeButtonProps,
} from 'Core/Components/Buttons/LargeButton';
import SquareBackBtn from 'Core/Components/Buttons/SquareBack';
import { useCustomSafeArea } from 'hooks/Actions/Style/useCustomSafeArea';
import React, {
  useCallback,
  useImperativeHandle,
  useMemo,
  useState,
} from 'react';
import {
  GestureResponderEvent,
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

export type OptionsType = {
  header?: React.ReactNode;
  backgroundOpacity?: number;
  buttons: Array<LargeButtonProps>;
};

export type ActionSheetViewImperativeHandle = {
  setOptions: (options: OptionsType) => any;
  open: () => any;
  close: () => any;
};

function ActionSheetView(props: any, ref: any) {
  const defaultOptions = useMemo(
    () => ({ backgroundOpacity: 0, buttons: [] }),
    [],
  );
  const [displayModal, setDisplayModal] = useState(false);
  const [options, setOptions] = useState<OptionsType>(defaultOptions);
  const insets = useCustomSafeArea();

  const close = useCallback(() => {
    setDisplayModal(false);
    setOptions(defaultOptions);
  }, [setDisplayModal, setOptions, defaultOptions]);

  useImperativeHandle(
    ref,
    () => ({
      setOptions,
      open: () => {
        setDisplayModal(true);
      },
      close,
    }),
    [close],
  );

  const { backgroundOpacity = 0, header } = options;
  return (
    <Modal animationType="slide" visible={displayModal} transparent={true}>
      <TouchableWithoutFeedback
        onPress={() => {
          close();
        }}>
        <View
          style={[
            styles.container,
            {
              backgroundColor: `rgba(0, 0, 0, ${backgroundOpacity})`,
              paddingBottom: insets.bottom,
            },
          ]}>
          {!!header && <View style={styles.headerContainer}>{header}</View>}
          {options.buttons.map((buttonProps, index) => (
            <LargeButton
              key={index}
              {...buttonProps}
              onPress={(e: GestureResponderEvent) => {
                let onPressResult: any = null;
                if (buttonProps.onPress) {
                  onPressResult = buttonProps.onPress(e);
                }
                if (onPressResult && onPressResult.then) {
                  onPressResult.then((...args: any) => {
                    close();
                    return args;
                  });
                } else {
                  close();
                }
              }}
              variant={buttonProps.variant || 'gray'}
              style={[styles.button, buttonProps.style]}
            />
          ))}

          <SquareBackBtn
            direction="bottom"
            onPress={() => {
              setDisplayModal(false);
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
  },

  headerContainer: {
    marginBottom: 20,
    width: '100%',
  },

  button: {
    marginBottom: 10,
    width: '100%',
  },
});

export default React.forwardRef<ActionSheetViewImperativeHandle>(
  ActionSheetView,
);
