import ActionSheetView, {
  ActionSheetViewImperativeHandle,
  OptionsType,
} from 'Core/Components/ActionSheet/ActionSheet';
import * as React from 'react';

import { ActionSheetProps } from './types';

const context = React.createContext<ActionSheetProps>({
  showActionSheetWithOptions: (_options: OptionsType) => {},
  closeActionSheet: () => {},
});

export function useActionSheet() {
  return React.useContext(context);
}

const { Provider } = context;

export function ActionSheetProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const actionSheetRef = React.useRef<ActionSheetViewImperativeHandle>(null);

  return (
    <Provider
      value={{
        showActionSheetWithOptions: options => {
          if (!actionSheetRef.current) {
            return false;
          }
          actionSheetRef.current.setOptions(options);
          actionSheetRef.current.open();
        },
        closeActionSheet: () => {
          if (!actionSheetRef.current) {
            return false;
          }
          actionSheetRef.current.close();
        },
      }}>
      <ActionSheetView ref={actionSheetRef} />
      {children}
    </Provider>
  );
}
