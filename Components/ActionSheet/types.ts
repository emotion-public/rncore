import { OptionsType } from 'Core/Components/ActionSheet/ActionSheet';

export interface ActionSheetProps {
  showActionSheetWithOptions: (options: OptionsType) => void;
  closeActionSheet: () => void;
}
