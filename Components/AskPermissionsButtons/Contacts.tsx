import React, { useState } from 'react';
import PermissionButtonTemplate from 'Core/Components/AskPermissionsButtons/Template';
import { ViewStyle } from 'react-native';
import { useTranslation } from 'react-i18next';

export default function ContactsPermissionsButton({
  onGranted = () => {},
  style,
}: {
  onGranted?: () => any;
  style?: ViewStyle;
}) {
  const { t } = useTranslation();
  const [allowed, setAllowed] = useState<boolean>(false);

  const onPressAsk = () => {
    setAllowed(true);
    onGranted();
  };
  return (
    <PermissionButtonTemplate
      title={t('contactPermTitle')}
      subtitle={t('findYourFriends')}
      disabled={allowed}
      value={allowed}
      onChange={onPressAsk}
      style={style}
    />
  );
}
