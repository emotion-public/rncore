import React, { useEffect, useState } from 'react';
import PermissionButtonTemplate from 'Core/Components/AskPermissionsButtons/Template';
import { ViewStyle } from 'react-native';
import sharedRecorder from 'Core/AudioRecorder';
import { PERMISSIONS, usePermissionStatus } from 'Core/Permissions';
import { useTranslation } from 'react-i18next';

export default function MicrophonePermissionsButton({
  onAskedResult = () => {},
  style,
}: {
  onAskedResult?: (granted: boolean) => any;
  style?: ViewStyle;
}) {
  const { t } = useTranslation();
  const { permissionStatus: microphonePermissionsStatus } = usePermissionStatus(
    PERMISSIONS.MICROPHONE,
  );

  const [allowed, setAllowed] = useState<boolean>(
    microphonePermissionsStatus === 'granted',
  );

  useEffect(() => {
    if (microphonePermissionsStatus === 'granted') {
      setAllowed(true);
    }
  }, [microphonePermissionsStatus]);

  const onPressAsk = async () => {
    const { hasPermission } =
      await sharedRecorder.requestPermissionIfNecessary();
    if (hasPermission) {
      setAllowed(true);
    }

    onAskedResult(hasPermission);
  };
  return (
    <PermissionButtonTemplate
      title={t('microphonePermTitle')}
      subtitle={t('sendVoiceMessages')}
      disabled={allowed}
      value={allowed}
      onChange={onPressAsk}
      style={style}
    />
  );
}
