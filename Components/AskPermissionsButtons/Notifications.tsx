import React, { useState } from 'react';
import PermissionButtonTemplate from 'Core/Components/AskPermissionsButtons/Template';
import { ViewStyle } from 'react-native';
import usePromptNotifications from 'Core/Libs/OneSignal/usePromptNotifications';
import { useTranslation } from 'react-i18next';
import { LogEvent } from 'Core/Libs/Amplitude';

export default function NotificationsPermissionsButton({
  onAskedResult = () => {},
  style,
}: {
  onAskedResult?: (granted: boolean) => any;
  style?: ViewStyle;
}) {
  const { t } = useTranslation();
  const [allowed, setAllowed] = useState<boolean>(false);
  const promptForNotifs = usePromptNotifications();

  const onPressAsk = async () => {
    const response = await promptForNotifs();
    // console.log('response', response);
    if (response) {
      LogEvent('Answer Push Permissions');
      setAllowed(true);
    }
    onAskedResult(response);
  };
  return (
    <PermissionButtonTemplate
      title={t('notifsPermTitle')}
      subtitle={t('receiveVoiceMessages')}
      disabled={allowed}
      value={allowed}
      onChange={onPressAsk}
      style={style}
    />
  );
}
