import FONTS from 'Constants/Fonts';
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Switch,
  Text,
  View,
  ViewStyle,
} from 'react-native';
import { scale } from 'Core/Layout/ScaleHelpers';

export default function PermissionButtonTemplate({
  title,
  subtitle,
  value,
  disabled = false,
  onChange,
  style,
}: {
  title: string;
  subtitle: string;
  value?: boolean;
  disabled?: boolean;
  onChange?: (value: boolean) => any;
  style?: ViewStyle;
}) {
  return (
    <TouchableOpacity
      onPress={onChange}
      activeOpacity={1}
      style={[styles.container, style]}>
      <View style={styles.titleContainer}>
        <View style={styles.titleIconContainer}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <Text style={styles.subtitle}>{subtitle}</Text>
      </View>
      <View style={styles.btnContainer}>
        <Switch
          disabled={disabled}
          trackColor={{ false: 'rgba(255, 255, 255, 0.3)', true: '#8D11EE' }}
          thumbColor={'white'}
          onValueChange={onChange}
          value={value}
        />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    borderRadius: scale(30),
    height: scale(80),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 22,
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  titleIconContainer: {
    flexDirection: 'row',
  },
  title: {
    fontFamily: FONTS.common.secondary,
    color: 'white',
    fontSize: scale(18),
    fontWeight: '700',
    fontStyle: 'normal',
  },
  subtitle: {
    color: '#ACABAB',
    fontFamily: FONTS.common.secondary,
    fontSize: scale(13),
    fontWeight: '500',
    fontStyle: 'normal',
  },
  btnContainer: {
    height: 35,
    width: 54,
  },
});
