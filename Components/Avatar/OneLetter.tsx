import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';

export default function AvatarOneLetter({
  style,
  letter,
}: {
  style?: StyleProp<ViewStyle>;
  letter: string;
}) {
  return (
    <View style={[styles.container, style]}>
      <View style={[styles.insideCircle]}>
        <Text style={styles.letter}>{letter.substring(0, 1)}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: 40,
    borderRadius: 100,
    backgroundColor: 'rgba(84,84,84, 1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  insideCircle: {
    width: 30,
    height: 30,
    borderRadius: 100,
    backgroundColor: 'rgba(41,41,41, 1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  letter: {
    fontSize: 13,
    fontWeight: '700',
    fontStyle: 'normal',
    textAlignVertical: 'center',
    color: '#FFFFFF',
  },
});
