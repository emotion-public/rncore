import { range } from 'Core/Helpers/NumberInterpolations';
import { scale } from 'Core/Layout/ScaleHelpers';
import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';
import FastImage, { Source } from 'react-native-fast-image';

export type AvatarSquareProps = {
  style?: StyleProp<ViewStyle>;
  borderColor?: string;
  letter?: string;
  source?: Source;
  imgUrl?: string;
  borderRadius?: number;
  shadowStyle?: 'dark' | 'light' | 'verylight';
  size?: number;
};

export default function AvatarSquare({
  style,
  borderColor,
  imgUrl,
  source,
  letter,
  borderRadius,
  shadowStyle = 'dark',
  size,
}: AvatarSquareProps) {
  let width = parseInt(
    StyleSheet.flatten(style)?.width?.toString() || '80',
    10,
  );
  let height = 80;
  if (size) {
    width = size;
    height = size;
  }
  const borderWidth = range(80, 131, 6.5, 8, width);

  const hasImage = !!source || (!!imgUrl && imgUrl !== '');

  return (
    <View style={[styles.container]}>
      <View
        style={[
          styles.avatarContainer,
          { width, height },
          borderRadius ? { borderRadius } : null,
          style,
        ]}>
        <View
          style={[
            styles.shadowInset,
            !hasImage ? styles.shadowInsetVeryLight : null,
            borderRadius ? { borderRadius } : null,
            borderColor ? { borderColor } : null,
            { borderWidth },
            shadowStyle === 'light' ? styles.shadowInsetLight : null,
            shadowStyle === 'verylight' ? styles.shadowInsetVeryLight : null,
          ]}
        />
        {hasImage ? (
          <FastImage
            source={source || { uri: imgUrl }}
            style={styles.contentContainer}
          />
        ) : letter ? (
          <View
            style={[
              styles.letterContainer,
              { width, height },
              borderRadius ? { borderRadius } : null,
            ]}>
            <Text style={[styles.letter, size ? { fontSize: size / 2 } : null]}>
              {letter}
            </Text>
          </View>
        ) : null}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {},
  avatarContainer: {
    width: 80,
    height: 80,
    borderRadius: scale(20),
    overflow: 'hidden',
  },

  shadowInset: {
    position: 'absolute',
    zIndex: 2,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    borderRadius: scale(20),
    shadowColor: 'rgb(0, 0, 0, 0.13)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 13,
    borderWidth: 7,
    borderColor: 'rgba(0, 0, 0, 0.5)',
  },
  shadowInsetLight: {
    borderColor: 'rgba(255, 255, 255, 0.5)',
  },
  shadowInsetVeryLight: {
    borderColor: 'rgba(255, 255, 255, 0.21)',
  },
  contentContainer: {
    flex: 1,
  },
  letterContainer: {
    borderRadius: 28,
    height: 80,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  letter: {
    fontFamily: 'Avenir Next',
    textTransform: 'uppercase',
    fontSize: 50,
    // lineHeight: 85,
    textAlign: 'center',
    fontWeight: '700',
    fontStyle: 'normal',
    color: '#FFFFFF',
    textAlignVertical: 'center',
  },
});
