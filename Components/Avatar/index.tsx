import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';
import FastImage from 'react-native-fast-image';
import Animated from 'react-native-reanimated';

export default function Avatar({
  style,
  shadowInsetStyle,
  letter,
  imgUrl,
}: {
  style?: StyleProp<ViewStyle>;
  shadowInsetStyle?: StyleProp<ViewStyle>;
  letter?: string;
  imgUrl?: string;
}) {
  return (
    <Animated.View style={[styles.container, style]}>
      <Animated.View style={[styles.shadowInset, shadowInsetStyle]} />

      {imgUrl && imgUrl !== '' ? (
        <FastImage source={{ uri: imgUrl }} style={styles.contentContainer} />
      ) : letter ? (
        <View style={[styles.contentContainer, styles.textContainer]}>
          <Text style={styles.letter}>{letter.substring(0, 1)}</Text>
        </View>
      ) : null}
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 40,
    height: 40,
    borderRadius: 100,
  },

  shadowInset: {
    position: 'absolute',
    zIndex: 2,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    borderRadius: 100,
    shadowColor: 'rgb(0, 0, 0, 0.13)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 13,
    borderWidth: 4,
    borderColor: 'rgba(255, 255, 255, 0.5)',
  },
  contentContainer: {
    flex: 1,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

  textContainer: {
    backgroundColor: 'rgba(41,41,41, 1)',
  },

  letter: {
    fontSize: 13,
    fontWeight: '700',
    fontStyle: 'normal',
    textAlignVertical: 'center',
    color: '#FFFFFF',
  },
});
