import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Back from '../../Icons/back.svg';

export default function BackBtn(
  props: TouchableOpacity['props'] & { fill: string },
) {
  return (
    <TouchableOpacity {...props} style={[styles.container, props.style]}>
      <Back height={22} fill={props.fill || 'white'} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
