import COLORS from 'Constants/Colors';
import FONTS from 'Constants/Fonts';
import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const textColorVariantWhite = '#ffffff';
const textColorVariantBlack = '#000000';
const textColorVariantGray = '#FFFFFF99';

export type LargeButtonProps = {
  title: string;
  textColor?: string;
  loading?: boolean;
  iconLeft?: React.ReactNode;
  variant?:
    | 'white'
    | 'flatWhite'
    | 'purple'
    | 'blue'
    | 'red'
    | 'gray'
    | 'richWhite';
} & TouchableOpacityProps;
export default function LargeButton({
  title,
  textColor,
  loading = false,
  iconLeft,
  variant = 'white',
  gradient = true,
  children: _,
  style,
  ...props
}: LargeButtonProps) {
  let variantStyle = null;
  let textColorVariant = textColorVariantWhite;
  switch (variant) {
    case 'white':
      // variantStyle = styles.variantWhite;
      textColorVariant = textColorVariantBlack;
      break;
    case 'flatWhite':
      variantStyle = styles.variantFlatWhite;
      textColorVariant = textColorVariantBlack;
      break;
    case 'richWhite':
      variantStyle = styles.variantFlatWhite;
      textColorVariant = textColorVariantBlack;
      break;
    case 'purple':
      variantStyle = styles.variantPurple;
      break;
    case 'blue':
      variantStyle = styles.variantBlue;
      break;
    case 'red':
      variantStyle = styles.variantRed;
      break;
    case 'gray':
      variantStyle = styles.variantGray;
      textColorVariant = textColorVariantGray;
      break;
    default:
      break;
  }

  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.container,
        variantStyle,
        iconLeft ? styles.withIcon : null,
        style,
      ]}>
      {gradient && (
        <LinearGradient
          colors={[COLORS.gradient.white_0, COLORS.gradient.black_50]}
          style={styles.linearGradient}
        />
      )}

      {iconLeft && <View style={styles.iconContainer}>{iconLeft}</View>}
      {!loading && (
        <Text
          style={[
            styles.title,
            { color: textColorVariant },
            textColor ? { color: textColor } : null,
          ]}>
          {title}
        </Text>
      )}
      {loading && <ActivityIndicator color={textColorVariant} />}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 72,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  linearGradient: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  withIcon: {
    justifyContent: 'flex-start',
  },

  iconContainer: {
    marginLeft: 16,
    marginRight: 10,
  },

  variantWhite: {
    backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
  },

  variantGray: {
    backgroundColor: COLORS.common.gray,
  },

  variantFlatWhite: {
    backgroundColor: '#FFFFFF',
  },

  variantPurple: {
    backgroundColor: COLORS.common.purpleAlt,
  },
  variantBlue: {
    backgroundColor: COLORS.common.blue,
  },
  variantRed: {
    backgroundColor: COLORS.common.red,
  },

  title: {
    fontFamily: FONTS.common.primary,
    fontSize: 20,
    fontWeight: '700',
    fontStyle: 'normal',
    lineHeight: 20,
    color: '#FFFFFF',
  },

  textColorVariantGray: {
    color: COLORS.common.lightGray,
  },
  textColorVariantWhite: {
    color: '#000000',
  },
});
