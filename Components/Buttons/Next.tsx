import SquareBtn, { SquareBtnProps } from 'Core/Components/Buttons/SquareBtn';
import React from 'react';
import { ActivityIndicator } from 'react-native';
import ArrowForward from '../../Icons/arrow-forward.svg';

export default function NextBtn(props: SquareBtnProps & { loading?: boolean }) {
  return (
    <SquareBtn {...props}>
      {props.loading ? (
        <ActivityIndicator color={'#ffffff'} />
      ) : (
        <ArrowForward height={22} fill={'white'} />
      )}
    </SquareBtn>
  );
}
