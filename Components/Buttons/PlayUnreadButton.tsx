import COLORS from 'Constants/Colors';
import FONTS from 'Constants/Fonts';
import React from 'react';
import PlayIcon from 'Core/Icons/play.svg';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';

export type PlayUnreadButtonProps = {
  count?: number;
  title?: string;
} & TouchableOpacityProps;

export default function PlayUnreadButton({
  count,
  title,
}: PlayUnreadButtonProps) {
  return (
    <TouchableOpacity style={[styles.container]}>
      <View style={styles.counterContainer}>
        <Text style={styles.counter}>{count}</Text>
      </View>
      <View style={styles.labelContainer}>
        <Text style={[styles.title]}>{title}</Text>
        <PlayIcon fill={'white'} height={20} />
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 72,
    borderRadius: 28,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.common.red,
    paddingHorizontal: 12,
    paddingVertical: 10,
    paddingRight: 20,
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  title: {
    fontFamily: FONTS.common.primary,
    fontSize: 24,
    fontWeight: '700',
    fontStyle: 'normal',
    // lineHeight: 20,
    color: '#FFFFFF',
    paddingHorizontal: 10,
  },
  counterContainer: {
    backgroundColor: COLORS.common.black,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 18,
  },
  counter: {
    fontStyle: 'normal',
    color: COLORS.common.red,
    alignItems: 'center',
    fontFamily: FONTS.common.tertiary,
    fontSize: 40,
    paddingHorizontal: 18,
  },
});
