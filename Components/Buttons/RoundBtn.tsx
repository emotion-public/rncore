import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';

export type RoundBtnProps = TouchableOpacity['props'];

export default function RoundBtn({
  iconContainerStyle,
  ...props
}: TouchableOpacity['props'] & {
  loading?: boolean;
  iconContainerStyle?: StyleProp<ViewStyle>;
}) {
  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.container,
        props.disabled && !props.loading ? styles.disabled : {},
        props.style,
      ]}>
      <View style={[styles.iconContainer, iconContainerStyle]}>
        {props.children}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 70,
    borderRadius: 100,
    backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 3.74,
    },
    shadowRadius: 3.740259885787964,
    shadowOpacity: 1,
    borderStyle: 'solid',
    borderColor: 'rgba(255, 255, 255, 0.68)',
  },
  disabled: {
    opacity: 0.4,
  },
  iconContainer: {
    width: 70,
    height: 70,
    borderRadius: 100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
