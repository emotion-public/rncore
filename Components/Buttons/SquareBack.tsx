import SquareBtn, { SquareBtnProps } from 'Core/Components/Buttons/SquareBtn';
import React from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import ChevronDown from '../../Icons/chevron-down.svg';
import ChevronBack from '../../Icons/chevron-back.svg';

export default function SquareBackBtn({
  direction = 'bottom',
  ...props
}: SquareBtnProps & { direction?: 'left' | 'bottom'; loading?: boolean }) {
  return (
    <SquareBtn
      {...props}
      style={[styles.squareBtn, props.style]}
      iconContainerStyle={styles.squareBtnIconContainer}>
      {props.loading ? (
        <ActivityIndicator color={'#ffffff'} />
      ) : direction === 'bottom' ? (
        <ChevronDown height={22} fill={'white'} />
      ) : (
        <ChevronBack height={22} fill={'white'} />
      )}
    </SquareBtn>
  );
}

const styles = StyleSheet.create({
  squareBtn: {
    backgroundColor: 'rgba(51,51,51,1)',
  },
  squareBtnIconContainer: {
    backgroundColor: 'rgba(92,	92,	92, 1)',
  },
});
