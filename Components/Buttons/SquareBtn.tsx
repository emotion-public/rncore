import React from 'react';
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import COLORS from 'Constants/Colors';

export type SquareBtnProps = TouchableOpacity['props'] & {
  variant?: 'purple' | 'gray';
  loading?: boolean;
  iconContainerStyle?: StyleProp<ViewStyle>;
};

export default function SquareBtn({
  variant = 'purple',
  gradient = false,
  iconContainerStyle,
  ...props
}: SquareBtnProps) {
  return (
    <TouchableOpacity
      {...props}
      style={[
        styles.container,
        variant === 'gray' ? styles.variantGray : null,
        props.disabled && !props.loading ? styles.disabled : {},
        props.style,
      ]}>
      {gradient && (
        <LinearGradient
          colors={[COLORS.gradient.white_0, COLORS.gradient.black_40]}
          style={[
            styles.container,
            variant === 'gray' ? styles.variantGray : null,
            props.disabled && !props.loading ? styles.disabled : {},
            styles.linearGradient,
          ]}
        />
      )}
      <View
        style={[
          styles.iconContainer,
          variant === 'gray' ? styles.variantGrayIconContainer : null,
          iconContainerStyle,
        ]}>
        {props.children}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 28,
    padding: 18,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(141, 17, 238, 1)',
    width: 96,
    height: 76,
  },
  variantGray: {
    backgroundColor: 'rgba(51,51,51,1)',
  },
  variantGrayIconContainer: {
    backgroundColor: 'rgba(92,	92,	92, 1)',
  },
  linearGradient: {
    flex: 1,
    position: 'absolute',
  },
  disabled: {
    opacity: 0.4,
  },
  iconContainer: {
    width: 40,
    height: 40,
    borderRadius: 100,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
