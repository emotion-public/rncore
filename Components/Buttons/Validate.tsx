import SquareBtn, { SquareBtnProps } from 'Core/Components/Buttons/SquareBtn';
import React from 'react';
import { ActivityIndicator } from 'react-native';
import Check from '../../Icons/check.svg';

export default function ValidateBtn(
  props: SquareBtnProps & { loading?: boolean },
) {
  return (
    <SquareBtn {...props}>
      {props.loading ? (
        <ActivityIndicator color={'#ffffff'} />
      ) : (
        <Check height={22} fill={'white'} />
      )}
    </SquareBtn>
  );
}
