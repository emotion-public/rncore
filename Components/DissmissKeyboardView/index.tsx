import React from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';

export default function DissmissKeyboardView({
  onPress,
  ...props
}: TouchableWithoutFeedback['props']) {
  return (
    <TouchableWithoutFeedback
      {...props}
      onPress={(e: any) => {
        Keyboard.dismiss();
        if (onPress) {
          onPress(e);
        }
      }}
    />
  );
}
