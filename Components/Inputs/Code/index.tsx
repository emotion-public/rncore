import FONTS from 'Constants/Fonts';
import React, { useRef, useState } from 'react';
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

function Cell({ value }: { value: string }) {
  return (
    <View style={styles.cell}>
      {value ? (
        <Animatable.Text animation="bounceIn" style={[styles.cellInput]}>
          {value}
        </Animatable.Text>
      ) : (
        <View style={[styles.cellPlaceholder]} />
      )}
    </View>
  );
}

export default function InputCode({
  style,
  size = 4,
  onChange,
  value: userValue,
}: {
  style?: ViewStyle;
  size?: number;
  value?: string;
  onChange?: (value: string) => any;
}) {
  const inputRef = useRef<TextInput>(null);
  const [_value, setValue] = useState<string>('');

  const value = userValue || _value;

  const onPress = () => {
    inputRef.current?.focus();
  };
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.container, style]}
      onPress={onPress}>
      <TextInput
        autoFocus
        keyboardAppearance="dark"
        ref={inputRef}
        style={[styles.hiddenInput]}
        selectionColor={'white'}
        caretHidden
        value={String(value)}
        keyboardType="decimal-pad"
        onChange={({ nativeEvent: { text: _text } }) => {
          const text = _text.trim();
          if (text === '') {
            setValue('');
            if (onChange) {
              onChange('');
            }
            return;
          }
          if (
            String(parseInt(text[text.length - 1], 10)) !==
            text[text.length - 1]
          ) {
            return;
          }
          if (text.length > size) {
            return;
          }
          setValue(text);
          if (onChange) {
            onChange(text);
          }
        }}
      />
      <View style={styles.cellContainer}>
        {[...Array(size).keys()].map((i: number) => (
          <Cell key={i} value={value[i] || ''} />
        ))}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    textAlign: 'left',
    width: 295,
    height: 65,
    borderRadius: 26,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    color: 'white',

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  cellContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  hiddenInput: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
    color: 'transparent',
    fontSize: 28,
    textAlign: 'center',
    width: 16,
  },
  cell: {
    marginHorizontal: 10,
  },
  cellInput: {
    width: 16,
    fontSize: 25,
    lineHeight: 31,
    height: 28,
    color: 'white',
    fontFamily: FONTS.common.secondary,
    fontWeight: '700',
  },
  cellPlaceholder: {
    width: 12,
    height: 12,
    marginHorizontal: 2,
    borderRadius: 100,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
});
