import COLORS from 'Constants/Colors';
import React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  TouchableOpacityProps,
  View,
} from 'react-native';

const textColorVariantWhite = '#ffffff';
const textColorVariantBlack = '#000000';
const textColorVariantGray = '#FFFFFF99';

const placeholderColorVariantWhite = 'rgba(255,255,255,0.5)';
const placeholderColorVariantBlack = 'rgba(0,0,0,0.5)';
const placeholderColorVariantGray = 'rgba(255,255,255,0.5)';

export type LargeInputProps = {
  value: string;
  onChangeText: (text: string) => any;
  textColor?: string;
  loading?: boolean;
  iconLeft?: React.ReactNode;
  variant?: 'white' | 'flatWhite' | 'purple' | 'blue' | 'red' | 'gray';
  textInputProps?: TextInput['props'];
} & TouchableOpacityProps;
export default function LargeInput({
  value,
  onChangeText,
  textColor,
  loading = false,
  iconLeft,
  variant = 'white',
  children: _,
  style,
  textInputProps,
  ...props
}: LargeInputProps) {
  let variantStyle = null;
  let textColorVariant = textColorVariantWhite;
  let placeholderColorVariant = placeholderColorVariantWhite;
  switch (variant) {
    case 'white':
      variantStyle = styles.variantWhite;
      textColorVariant = textColorVariantBlack;
      placeholderColorVariant = placeholderColorVariantBlack;
      break;
    case 'flatWhite':
      variantStyle = styles.variantFlatWhite;
      textColorVariant = textColorVariantBlack;
      placeholderColorVariant = placeholderColorVariantBlack;
      break;
    case 'purple':
      variantStyle = styles.variantPurple;
      break;
    case 'blue':
      variantStyle = styles.variantBlue;
      break;
    case 'red':
      variantStyle = styles.variantRed;
      break;
    case 'gray':
      variantStyle = styles.variantGray;
      textColorVariant = textColorVariantGray;
      placeholderColorVariant = placeholderColorVariantGray;
      break;
    default:
      break;
  }

  return (
    <TouchableOpacity
      {...props}
      activeOpacity={1}
      style={[
        styles.container,
        variantStyle,
        iconLeft ? styles.withIcon : null,
        style,
      ]}>
      {iconLeft && <View style={styles.iconContainer}>{iconLeft}</View>}
      {!loading && (
        <TextInput
          {...textInputProps}
          value={value}
          keyboardType={'ascii-capable'}
          onChangeText={onChangeText}
          placeholderTextColor={placeholderColorVariant}
          style={[
            styles.input,
            { color: textColorVariant },
            textColor ? { color: textColor } : null,
          ]}
        />
      )}
      {loading && <ActivityIndicator color={textColorVariant} />}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 72,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  withIcon: {
    justifyContent: 'flex-start',
  },

  iconContainer: {
    marginLeft: 16,
    marginRight: 17,
  },

  variantWhite: {
    backgroundColor: '#FFFFFF',
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
  },

  variantGray: {
    backgroundColor: COLORS.common.gray,
  },

  variantFlatWhite: {
    backgroundColor: '#FFFFFF',
  },

  variantPurple: {
    backgroundColor: COLORS.common.purpleAlt,
  },
  variantBlue: {
    backgroundColor: COLORS.common.blue,
  },
  variantRed: {
    backgroundColor: COLORS.common.red,
  },

  input: {
    fontFamily: 'Gill Sans',
    fontSize: 20,
    fontWeight: '700',
    fontStyle: 'normal',
    lineHeight: 20,
    color: '#FFFFFF',
    height: '90%',
    flex: 1,
  },

  textColorVariantGray: {
    color: COLORS.common.lightGray,
  },
  textColorVariantWhite: {
    color: '#000000',
  },
});
