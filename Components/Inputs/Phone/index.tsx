import React, { useRef, useState } from 'react';
import parsePhoneNumber from 'libphonenumber-js';
import PhoneInput from 'react-native-phone-number-input';
import { StyleSheet } from 'react-native';
import FONTS from 'Constants/Fonts';
import { LogEvent } from 'Core/Libs/Amplitude';
import { scale } from 'Core/Layout/ScaleHelpers';

export default function PhoneNumberInput({
  onValid,
  onChangeValidPhoneNumber,
}: {
  onValid?: (valid: boolean) => any;
  onChangeValidPhoneNumber?: (e164PhoneNumber: string) => any;
}) {
  const phoneInput = useRef<PhoneInput>(null);
  const [value, setValue] = useState('');
  return (
    <PhoneInput
      ref={phoneInput}
      defaultValue={value}
      defaultCode="FR"
      onChangeText={text => {
        setValue(text);
      }}
      onChangeCountry={() => {
        LogEvent('Open Flags Modal');
      }}
      onChangeFormattedText={text => {
        const phoneNumber = parsePhoneNumber(text);
        const e164Value = phoneNumber?.format('E.164') || '';

        if (phoneNumber && phoneNumber.isValid()) {
          if (onValid) {
            onValid(true);
          }
          if (onChangeValidPhoneNumber) {
            onChangeValidPhoneNumber(e164Value);
          }
        } else {
          if (onValid) {
            onValid(false);
          }
          if (onChangeValidPhoneNumber) {
            onChangeValidPhoneNumber('');
          }
        }
      }}
      withDarkTheme
      withShadow={false}
      disableArrowIcon
      containerStyle={styles.containerStyle}
      codeTextStyle={styles.codeTextStyle}
      flagButtonStyle={styles.flagButtonStyle}
      textContainerStyle={styles.textContainerStyle}
      textInputProps={{
        selectionColor: 'rgba(255, 255, 255, 0.5)',
        placeholder: '2345678900',
        placeholderTextColor: 'rgba(255, 255, 255, 0.2)',
      }}
      textInputStyle={styles.textInputStyle}
    />
  );
}

const styles = StyleSheet.create({
  containerStyle: {
    height: 65,
    borderRadius: 26,
    overflow: 'hidden',
    paddingLeft: 15,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    alignItems: 'center',
  },

  codeTextStyle: {
    fontFamily: FONTS.common.secondary,
    fontWeight: '700',
    color: 'white',
    fontSize: 19,
    marginLeft: 4,
    height: 30,
    lineHeight: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  flagButtonStyle: {
    width: scale(28),
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingRight: 0,
    marginRight: 0,
  },

  textContainerStyle: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 0,
    height: 50,
  },

  textInputStyle: {
    fontFamily: FONTS.common.secondary,
    fontWeight: '700',
    height: 40,
    fontSize: 20,
    color: 'white',
    backgroundColor: 'transparent',
  },
});
