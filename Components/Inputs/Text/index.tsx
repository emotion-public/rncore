import FONTS from 'Constants/Fonts';
import React from 'react';
import {
  StyleSheet,
  TextInput as BaseTextInput,
  View,
  ViewStyle,
} from 'react-native';

export default function TextInput({
  iconLeft,
  containerStyle,
  style,
  ...props
}: {
  iconLeft?: React.ReactNode;
  containerStyle?: ViewStyle;
} & BaseTextInput['props']) {
  return (
    <View style={[styles.container, containerStyle]}>
      {!!iconLeft && <View style={styles.iconLeftContainer}>{iconLeft}</View>}
      <BaseTextInput
        {...props}
        keyboardAppearance="dark"
        selectionColor={'white'}
        style={[styles.input, style]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 65,
    paddingHorizontal: 13,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    borderRadius: 26,
  },
  iconLeftContainer: {
    width: 40,
    height: 50,
    marginRight: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    backgroundColor: 'transparent',
    color: 'white',
    borderWidth: 0,
    flex: 1,
    height: 35,
    fontSize: 21,
    fontWeight: 'bold',
    fontFamily: FONTS.common.secondary,
  },
});
