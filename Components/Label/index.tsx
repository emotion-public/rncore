import FONTS from 'Constants/Fonts';
import { scale } from 'Core/Layout/ScaleHelpers';
import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';

export default function Label({
  title,
  icon,
  displayIconBackground = true,
  style,
}: {
  title: string;
  icon?: React.ReactNode;
  displayIconBackground?: boolean;
  style?: StyleProp<ViewStyle>;
}) {
  return (
    <View style={[styles.container, style]}>
      {icon && (
        <View
          style={[
            styles.iconContainer,
            displayIconBackground ? styles.iconBackground : null,
          ]}>
          {icon}
        </View>
      )}

      <Text style={[styles.title, icon ? styles.titleWithIcon : null]}>
        {title}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: scale(30, 0.3),
    borderRadius: 12,
    backgroundColor: 'rgb(49,49,49)',
    paddingHorizontal: 6,
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-start',
  },
  iconContainer: {
    paddingVertical: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconBackground: {
    paddingHorizontal: 2,
    borderRadius: 8,
    backgroundColor: 'rgb(70,70,70)',
  },
  title: {
    fontSize: 12,
    fontWeight: '900',
    fontStyle: 'normal',
    color: '#FFFFFF99',
    fontFamily: FONTS.common.secondary,
  },
  titleWithIcon: {
    marginLeft: 4,
  },
});
