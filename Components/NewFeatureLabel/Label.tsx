import FONTS from 'Constants/Fonts';
import { scale } from 'Core/Layout/ScaleHelpers';
import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';

export default function Label({
  text,
  style,
}: {
  text?: string;
  style?: StyleProp<ViewStyle>;
}) {
  return (
    <View
      style={[
        styles.container,
        text ? styles.textLabel : styles.chipLabel,
        style,
      ]}>
      {!!text && <Text style={styles.text}>{text}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 100,
    backgroundColor: '#F92B64',
  },
  chipLabel: {
    width: scale(12),
    height: scale(12),
  },
  textLabel: {
    // width: scale(30),
    borderRadius: 7,
    paddingHorizontal: 5,
    paddingVertical: 2,
  },

  text: {
    textAlign: 'center',
    fontFamily: FONTS.common.secondary,
    fontWeight: 'bold',
    fontSize: scale(8),
    color: 'white',
  },
});
