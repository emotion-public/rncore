import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import featureList from 'Core/featureList';
import Label from 'Core/Components/NewFeatureLabel/Label';
import useNewFeatureLabelState from 'Core/hooks/useNewFeatureLabelState';
import useNewFeatureLabelActions from 'Core/hooks/useNewFeatureLabelActions';

export default function NewFeatureLabel({
  featureName,
  scale = 1,
  labelType = 'chip',
  position = 'topRight',
  triggerOn,
  style,
  children,
  ...props
}: {
  scale?: number;
  labelType?: 'chip' | 'text';
  triggerOn: 'view' | 'press';
  position?:
    | 'topRight'
    | 'right'
    | 'bottomRight'
    | 'left'
    | 'bottomLeft'
    | 'topLeft';
  featureName: keyof typeof featureList;
} & View['props']) {
  const feature = featureList[featureName];
  const date = new Date();
  const endDate = new Date(feature.endDate);
  const newFeatureLabelState = useNewFeatureLabelState();
  const [viewedLocalState] = useState<boolean>(
    newFeatureLabelState[featureName] ?? false,
  );

  const { setNewFeatureLabelViewed } = useNewFeatureLabelActions();

  useEffect(() => {
    if (triggerOn === 'view') {
      setNewFeatureLabelViewed(featureName);
    }
  }, [featureName, triggerOn, setNewFeatureLabelViewed]);

  let viewed = viewedLocalState;
  if (triggerOn === 'press') {
    viewed = newFeatureLabelState[featureName] ?? false;
  }

  viewed = false;

  return (
    <View
      style={[styles.container, style]}
      {...props}
      onTouchEnd={() => {
        if (triggerOn === 'press') {
          setNewFeatureLabelViewed(featureName);
        }
      }}>
      {date.getTime() < endDate.getTime() && !viewed && (
        <Label
          style={[
            styles.label,
            styles[position || 'topRight'],
            { transform: [{ scale }] },
          ]}
          text={labelType === 'text' ? 'NEW' : undefined}
        />
      )}
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    overflow: 'visible',
  },
  label: {
    position: 'absolute',
    zIndex: 2,
  },

  topRight: {
    top: -2,
    left: '70%',
  },
  bottomRight: {},
  right: {},
  topLeft: {},
  bottomLeft: {},
  left: {
    right: '130%',
  },
});
