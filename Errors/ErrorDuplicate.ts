export default class ErrorDuplicate extends Error {
  public code = 'DUPLICATE';
  public fieldName?: string;

  constructor(fieldName: string) {
    super();
    this.fieldName = fieldName;
  }
}
