import dateFormat from 'dateformat';

export const displayDateFromTimestamp = (ts: number): string => {
  const date = new Date(ts * 1000);

  return dateFormat(date, 'dd/mm, HH:MM');
};
