function padLeftZero(num: number) {
  num = Math.floor(num);
  let displayTime = '';
  if (num < 10) {
    displayTime += '0';
    displayTime += num < 1 ? '0' : num;
  } else {
    displayTime += num;
  }

  return displayTime;
}

export default function numberToDisplayTime(timeInSeconds: number): string {
  let minutes = Math.floor(timeInSeconds / 60);
  let secondsLeft = timeInSeconds - 60 * minutes;

  return minutes + ':' + padLeftZero(secondsLeft);
}
