import { ModelObject } from 'Core/Models/ModelObject';

export default function extractNodesPagination<T extends ModelObject>(
  gqlObject: any,
  key: string,
  UseClass: { new (): T },
): {
  objects: Array<T>;
  hasMore: boolean;
  endCursor: string | null;
} | null {
  let objects = [];
  let hasMore = false;
  let endCursor: string | null = null;

  if (!gqlObject || !gqlObject[key]) {
    return null;
  }

  objects = gqlObject[key].edges.map((edge: any) => {
    const o = new UseClass();
    o.setRawData(edge.node);
    return o;
  });
  hasMore = gqlObject[key].pageInfo.hasNextPage;
  endCursor = gqlObject[key].pageInfo.endCursor;

  return {
    objects,
    hasMore,
    endCursor,
  };
}
