import React from 'react';
import { StyleProp, StyleSheet, ViewStyle } from 'react-native';
import EditIconBase from './edit.svg';

export default function EditIcon({ style }: { style?: StyleProp<ViewStyle> }) {
  return <EditIconBase style={[styles.icon, style]} />;
}

const styles = StyleSheet.create({
  icon: {
    transform: [{ rotate: '90deg' }],
  },
});
