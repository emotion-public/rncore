import * as React from 'react';
import { StyleSheet, View, StyleProp, ViewStyle } from 'react-native';
import { useCustomSafeArea } from 'hooks/Actions/Style/useCustomSafeArea';

export default function BottomContainer({
  style,
  children,
  safeArea = true,
}: {
  style?: StyleProp<ViewStyle>;
  children?: React.ReactNode;
  safeArea?: boolean;
}) {
  const insets = useCustomSafeArea();

  return (
    <View
      style={[
        styles.container,
        safeArea ? { paddingBottom: insets.bottom } : null,
        style,
      ]}>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
