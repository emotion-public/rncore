import Title from 'Core/Layout/Title';
import { useCustomSafeArea } from 'hooks/Actions/Style/useCustomSafeArea';
import * as React from 'react';
import { StyleSheet, View, StyleProp, ViewStyle } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default function Header({
  title,
  subtitle,
  style,
  iconLeft,
  iconRight,
  safeArea = true,
  linearGradientsProps,
}: {
  title?: string;
  subtitle?: string;
  style?: StyleProp<ViewStyle>;
  iconLeft?: React.ReactElement;
  iconRight?: React.ReactElement;
  safeArea?: boolean;
  linearGradientsProps?: LinearGradient['props'];
}) {
  const insets = useCustomSafeArea();

  const content = (
    <>
      <View style={styles.line}>
        <View style={styles.iconContainer}>
          {iconLeft ? iconLeft : <View style={styles.iconSizePlaceHolder} />}
        </View>
        <View pointerEvents="none" style={[styles.titleContainer]}>
          {!!title && <Title size="medium">{title}</Title>}
        </View>
        <View style={[styles.iconContainer, styles.iconContainerRight]}>
          {iconRight ? iconRight : <View style={styles.iconSizePlaceHolder} />}
        </View>
      </View>
      {!!subtitle && (
        <View style={styles.line}>
          <Title type="h2" style={styles.subtitle}>
            {subtitle}
          </Title>
        </View>
      )}
    </>
  );

  if (linearGradientsProps) {
    return (
      <LinearGradient
        {...linearGradientsProps}
        style={[
          styles.container,
          // eslint-disable-next-line react-native/no-inline-styles
          { paddingTop: safeArea ? insets.top : 0 },
          style,
        ]}>
        {content}
      </LinearGradient>
    );
  }

  return (
    <View
      style={[
        styles.container,
        // eslint-disable-next-line react-native/no-inline-styles
        { paddingTop: safeArea ? insets.top : 0 },
        style,
      ]}>
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  line: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    // flex: 1,
    // backgroundColor: 'orange',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  iconContainerRight: {
    justifyContent: 'flex-end',
  },
  iconSizePlaceHolder: {
    width: 40,
    backgroundColor: 'orange',
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'blue',
  },
  subtitle: {
    color: 'rgba(255, 255, 255, 0.6)',
    fontWeight: 'normal',
  },
  childrenContainer: {
    marginTop: 26,
  },
});
