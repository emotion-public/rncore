import DissmissKeyboardView from 'Core/Components/DissmissKeyboardView';
import { useTheme } from 'Core/Theme';
import React from 'react';
import {
  KeyboardAvoidingView,
  KeyboardAvoidingViewProps,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';

import { useCustomSafeArea } from 'hooks/Actions/Style/useCustomSafeArea';
export type PageLayoutProps = {
  dissmissKeyboard?: boolean;
  safeArea?: boolean;
  safeAreaBottom?: boolean | { min: number };
  style?: StyleProp<ViewStyle>;
  animated?: boolean;
  keyboardAvoidingViewProps?: KeyboardAvoidingViewProps;
} & View['props'];

export default function PageLayout({
  style,
  keyboardAvoidingViewProps,
  dissmissKeyboard = false,
  // keyboadAvoidingView =false,
  safeArea = false,
  safeAreaBottom = false,
  ...props
}: PageLayoutProps) {
  const customInsets = useCustomSafeArea();

  const theme = useTheme();

  let paddingBottom = customInsets.bottom;
  if (
    paddingBottom === 0 &&
    typeof safeAreaBottom !== 'boolean' &&
    safeAreaBottom.min
  ) {
    paddingBottom = safeAreaBottom.min;
  }

  let Page = (
    <View
      style={[
        styles.container,
        { backgroundColor: theme.app_background_color },
        safeArea ? { paddingTop: customInsets.top } : {},
        safeAreaBottom ? { paddingBottom } : {},
        style,
      ]}
      {...props}
    />
  );

  if (keyboardAvoidingViewProps) {
    Page = (
      <KeyboardAvoidingView {...keyboardAvoidingViewProps}>
        {Page}
      </KeyboardAvoidingView>
    );
  }

  if (dissmissKeyboard) {
    Page = <DissmissKeyboardView>{Page}</DissmissKeyboardView>;
  }

  return Page;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: 'black',
  },
});
