import { Dimensions } from 'react-native';

// const BASE_WIDTH = 375;
const BASE_HEIGHT = 812;

// const SCREEN_WITH = Dimensions.get('screen').width;
const SCREEN_HEIGHT = Dimensions.get('screen').height;

export function scale(size: number, factor: number = 0.5) {
  const scaledSize = size * (SCREEN_HEIGHT / BASE_HEIGHT);

  return size + (scaledSize - size) * factor;
}

export function verticalScale(pts: number) {
  return pts * (SCREEN_HEIGHT / BASE_HEIGHT);
}
