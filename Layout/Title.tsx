import FONTS from 'Constants/Fonts';
import React from 'react';
import { StyleProp, TextProps, TextStyle } from 'react-native';
import { StyleSheet, Text } from 'react-native';
import { scale } from 'Core/Layout/ScaleHelpers';

export default function Title({
  style,
  title,
  type = 'h1',
  children,
  size: forceSize,
  ...textProps
}: {
  style?: StyleProp<TextStyle>;
  type?: 'h1' | 'h2' | 'h3';
  title?: string;
  children?: string;
  size?: 'small' | 'medium' | 'big';
} & TextProps) {
  let sizeStyle = null;
  let size = type === 'h1' ? 'big' : 'small';
  if (forceSize) {
    size = forceSize;
  }

  if (size === 'medium') {
    sizeStyle = styles.medium;
  }
  if (size === 'small') {
    sizeStyle = styles.small;
  }
  return (
    <Text {...textProps} style={[styles.title, styles[type], style, sizeStyle]}>
      {children || title}
    </Text>
  );
}

const styles = StyleSheet.create({
  title: {
    color: 'white',
    fontSize: scale(28),
    fontWeight: 'bold',
    fontFamily: FONTS.common.primary,
  },

  h1: {},
  h2: {
    fontFamily: FONTS.common.secondary,
    fontSize: scale(14),
    fontWeight: '500',
  },
  h3: {
    fontFamily: FONTS.common.secondary,
  },

  medium: {
    fontSize: scale(22),
  },

  small: {
    fontSize: scale(14),
  },
});
