import { Amplitude } from '@amplitude/react-native';

const ampInstance = Amplitude.getInstance();

export function initAmplitude(API_KEY: string) {
  ampInstance.init(API_KEY);
}

export function setUserId(userUUID: string) {
  ampInstance.setUserProperties({
    userUUID,
  });
  ampInstance.setUserId(userUUID);
}

export async function setUserProperties(userProperties: {
  [key: string]: any;
}) {
  return await ampInstance.setUserProperties(userProperties);
}

export type LogEventPropertiesType = {
  [key: string]: string | number | boolean | undefined;
};

export function LogEvent(
  eventType: string,
  properties?: LogEventPropertiesType,
) {
  // console.log('LOGEVENT -- ', eventType);
  ampInstance.logEvent(eventType, properties);
}
