import OneSignal from 'react-native-onesignal';

export default function initOneSignal(appId: string) {
  OneSignal.setLogLevel(6, 0);
  OneSignal.setAppId(appId);
}
