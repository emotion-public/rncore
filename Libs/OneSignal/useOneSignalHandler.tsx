import { LogEvent } from 'Core/Libs/Amplitude';
import { EventEmitter } from 'eventemitter3';
import { useEffect } from 'react';
import OneSignal, { OSNotification } from 'react-native-onesignal';

const eventEmitter = new EventEmitter<'notification_received', any>();

export function useOnNotificationReceived(
  onNotification: (notification: OSNotification) => any,
) {
  useEffect(() => {
    eventEmitter.on('notification_received', onNotification);
    return () => {
      eventEmitter.off('notification_received', onNotification);
    };
  }, [onNotification]);
}

export default function useOneSignalHandler() {
  const sendNotifToEventEmitter = (notification: OSNotification) => {
    eventEmitter.emit('notification_received', notification);
  };

  useEffect(() => {
    //Method for handling notifications received while app in foreground
    OneSignal.setNotificationWillShowInForegroundHandler(
      notificationReceivedEvent => {
        LogEvent('Receive Push Notification');
        let notification = notificationReceivedEvent.getNotification();
        notificationReceivedEvent.complete(notification);
        sendNotifToEventEmitter(notification);
      },
    );

    //Method for handling notifications opened
    OneSignal.setNotificationOpenedHandler(notification => {
      LogEvent('Open Push Notification');
      sendNotifToEventEmitter(notification.notification);
    });
  }, []);
}
