import { EventEmitter } from 'eventemitter3';
import { useEffect, useState } from 'react';
import OneSignal, { DeviceState } from 'react-native-onesignal';

// 0 = NotDetermined, 1 = Denied, 2 = Authorized, 3 = Provisional, 4 = Ephemeral

export enum NOTIFICATION_PERMISSION_STATUS {
  NotDetermined = 0,
  Denied = 1,
  Authorized = 2,
  Provisional = 3,
  Ephemeral = 4,
}

const permissionObserverEventEmitter = new EventEmitter<
  'on_permission_changed',
  any
>();

let hasNotificationPermission: boolean = false;
let hasPrompt: boolean = false;
let notificationPermissionStatus: any = null;

export function getNotificationPermissionStatus() {
  return {
    hasPrompt,
    hasNotificationPermission,
    notificationPermissionStatus,
  };
}

export function useDeviceState(): {
  isReady: boolean;
  deviceState: DeviceState | null;
} {
  const [isReady, setIsReady] = useState<boolean>(false);
  const [deviceState, setDeviceState] = useState<DeviceState | null>(null);

  async function refreshState() {
    const _deviceState = await OneSignal.getDeviceState();

    setDeviceState(_deviceState);
    hasNotificationPermission =
      _deviceState?.hasNotificationPermission ?? false;
    notificationPermissionStatus = _deviceState?.notificationPermissionStatus;
  }

  const onPermissionChanged = async () => {
    await refreshState();
  };

  useEffect(() => {
    (async () => {
      await refreshState();
      setIsReady(true);
    })();
  }, []);

  useEffect(() => {
    permissionObserverEventEmitter.on(
      'on_permission_changed',
      onPermissionChanged,
    );
    return () => {
      permissionObserverEventEmitter.off(
        'on_permission_changed',
        onPermissionChanged,
      );
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { isReady, deviceState };
}

export function launchPermissionObserver() {
  OneSignal.addPermissionObserver(event => {
    hasPrompt = event.to.hasPrompted ?? false;
    permissionObserverEventEmitter.emit('on_permission_changed');
  });
}

export default function usePromptNotifications(): () => Promise<boolean> {
  return async () => {
    return new Promise(resolve => {
      OneSignal.promptForPushNotificationsWithUserResponse(
        (response: boolean) => {
          resolve(response);
        },
      );
    });
  };
}
