import 'reflect-metadata';

const autocastToType = (type: any, value: any): any => {
  if (value === null || value === undefined) {
    return value;
  }

  switch (true) {
    case type === String:
      return String(value);
    case type === Boolean:
      return Boolean(value);
    case type === Number:
      const returnVal = Number(value);
      if (isNaN(returnVal)) {
        return undefined;
      }
      return returnVal;

    case typeof type === 'function' &&
      type.constructor &&
      typeof type.constructor === 'function':
      if (value instanceof type) {
        return value;
      }
      const castedValue = new type();
      if (castedValue instanceof ModelObject && typeof value === 'object') {
        castedValue.setData(value);
      }
      return castedValue;
    default:
      return value;
  }
};

export const autocast = (target: any, propertyKey: string, _?: any) => {
  const values = new Map<any, any>();

  let type = Reflect.getMetadata('design:type', target, propertyKey);

  Object.defineProperty(target, propertyKey, {
    enumerable: true,
    configurable: true,
    set(initialValue: any) {
      Object.defineProperty(this, propertyKey, {
        set(value: any) {
          values.set(this, autocastToType(type, value));
        },
        get() {
          return values.get(this);
        },
        enumerable: true,
      });
      this[propertyKey] = initialValue;
    },
  });
};

export function enumerable(isEnumerable: boolean = true) {
  return function (target: any, propertyKey: string, _?: any) {
    Object.defineProperty(target, propertyKey, {
      set(value) {
        let descriptor =
          Object.getOwnPropertyDescriptor(this, propertyKey) || {};

        Object.defineProperty(this, propertyKey, {
          value,
          writable: true,
          configurable: true,
          ...descriptor,
          enumerable: isEnumerable,
        });
      },
      enumerable: isEnumerable,
      configurable: true,
    });
  };
}

export class ModelObject {
  setData(data: any) {
    return this.setRawData(data);
  }
  setRawData(data: any) {
    const objectProperties = Object.getOwnPropertyNames(this);
    Object.keys(data).forEach(key => {
      if (key === 'object') {
        return true;
      }

      if (objectProperties.indexOf(key) !== -1) {
        Object.assign(this, { [key]: data[key] });
      }
    });
  }
}
