import * as React from 'react';
import {
  NavigationContainer as BaseNavigationContainer,
  NavigationContainerProps,
} from '@react-navigation/native';

export default function NavigationContainer({
  children,
  ...props
}: {
  children: React.ReactNode;
} & NavigationContainerProps) {
  return (
    <BaseNavigationContainer {...props}>{children}</BaseNavigationContainer>
  );
}
