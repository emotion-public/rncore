import { useEffect, useState } from 'react';
import { AppState } from 'react-native';
import {
  check,
  checkNotifications,
  PERMISSIONS as RN_PERMISSIONS,
  PermissionStatus as RnPermissionStatus,
} from 'react-native-permissions';

export type PermissionStatus = RnPermissionStatus;

export enum PERMISSIONS {
  MICROPHONE = 'MICROPHONE',
  NOTIFICATIONS = 'NOTIFICATIONS',
}

export async function checkHasPermission(
  permission: PERMISSIONS,
): Promise<PermissionStatus> {
  if (!PERMISSIONS[permission]) {
    throw new Error('Not allowed');
  }

  if (permission === PERMISSIONS.NOTIFICATIONS) {
    const { status } = await checkNotifications();
    return status;
  }

  const permissionStatus = await check(RN_PERMISSIONS.IOS[permission]);

  return permissionStatus;
}

export function usePermissionStatus(permission: PERMISSIONS) {
  const [permissionStatus, setPermissionStatus] = useState<
    PermissionStatus | undefined
  >();
  const [isReady, setIsReady] = useState<boolean>();
  const [appState, setAppState] = useState<any>();

  useEffect(() => {
    (async () => {
      setPermissionStatus(await checkHasPermission(permission));
      setIsReady(true);
    })();
  }, [permission, appState]);

  useEffect(() => {
    const removeEventListener = AppState.addEventListener(
      'change',
      nextAppState => {
        setAppState(nextAppState);
      },
    );

    return () => {
      removeEventListener.remove();
    };
  }, [permission]);

  return { permissionStatus, isReady };
}

export async function test() {
  if ((await checkHasPermission(PERMISSIONS.MICROPHONE)) === 'blocked') {
    return true;
  }
}
