import { useState } from 'react';
import axios from 'axios';
import * as ImagePicker from 'expo-image-picker';
import * as Haptics from 'expo-haptics';
import { Alert, Linking, Platform } from 'react-native';
import ImageResizer from 'react-native-image-resizer';
import CONFIG from 'Config';
import { useTranslation } from 'react-i18next';

export const uploadImage = async (
  uri: string,
  options?: { onProgress?: (progress: number) => any },
) => {
  const { onProgress = (_: number) => {} } = options || {};
  const uriParts = uri.split('.');
  const fileType = uriParts[uriParts.length - 1];

  const formData = new FormData();
  formData.append('uploadfile', {
    // @ts-ignore
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });

  onProgress(1 / 100);

  const result = await axios.post(CONFIG.base_api_url + '/upload', formData, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: ({
      total,
      loaded,
    }: {
      total: number;
      loaded: number;
    }) => {
      onProgress((100 * loaded) / total);
    },
  });

  const data = result.data;

  return data.filename;
};

type PICTURE_PICKER_OPTIONS = {
  quality?: number;
  resize?: { width: number; height: number };
};
export default function usePhotoPickerUploader(
  onChange?: (pictureUri: string) => any,
): [
  pictureUri: string | undefined,
  actions: {
    launchCameraAsync: (options?: PICTURE_PICKER_OPTIONS) => Promise<any>;
    launchImageLibraryAsync: (options?: PICTURE_PICKER_OPTIONS) => Promise<any>;
    uploadImage: (onProgress?: (progress: number) => any) => Promise<any>;
  },
] {
  const [pictureUri, _setPictureUri] = useState<string>();
  const { t } = useTranslation();

  const setPictureUri = (_pictureUri: string) => {
    _setPictureUri(_pictureUri);
    if (onChange) {
      onChange(_pictureUri);
    }
  };
  return [
    pictureUri,
    {
      launchCameraAsync: async (options?: PICTURE_PICKER_OPTIONS) => {
        if (Platform.OS === 'ios') {
          Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
        }

        const permissions = await ImagePicker.getCameraPermissionsAsync();
        if (!permissions.granted) {
          if (!permissions.canAskAgain) {
            Alert.alert(
              t('go_to_settings_title'),
              t('go_to_settings_message_camera'),
              [
                {
                  text: 'Cancel',
                  onPress: () => {},
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    Linking.openSettings();
                  },
                },
              ],
            );
            return false;
          }
          const { status } = await ImagePicker.requestCameraPermissionsAsync();
          if (status !== 'granted') {
            return false;
          }
        }

        const { quality = 1 } = options || {};
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          quality,
          allowsEditing: true,
          aspect: [1, 1],
          base64: true,
        });

        if (result.cancelled) {
          return false;
        }

        let uri = result.uri;

        if (options?.resize) {
          const response = await ImageResizer.createResizedImage(
            result.uri,
            options.resize.width,
            options.resize.height,
            'JPEG',
            quality * 100,
            0,
            undefined,
            false,
            undefined,
          );
          uri = response.uri;
        }

        setPictureUri(uri);

        return uri;
      },

      launchImageLibraryAsync: async (options?: PICTURE_PICKER_OPTIONS) => {
        if (Platform.OS === 'ios') {
          Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Heavy);
        }

        const permissions = await ImagePicker.getMediaLibraryPermissionsAsync();
        if (!permissions.granted) {
          if (!permissions.canAskAgain) {
            Alert.alert(
              t('go_to_settings_title'),
              t('go_to_settings_message_media_library'),
              [
                {
                  text: 'Cancel',
                  onPress: () => {},
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    Linking.openSettings();
                  },
                },
              ],
            );
            return false;
          }
          const { status } =
            await ImagePicker.requestMediaLibraryPermissionsAsync();
          if (status !== 'granted') {
            return false;
          }
        }

        const { quality = 1 } = options || {};
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          quality,
          allowsEditing: true,
          aspect: [1, 1],
          base64: true,
        });

        if (result.cancelled) {
          return false;
        }

        let uri = result.uri;

        if (options?.resize) {
          const response = await ImageResizer.createResizedImage(
            result.uri,
            options.resize.width,
            options.resize.height,
            'JPEG',
            quality * 100,
            0,
            undefined,
            false,
            undefined,
          );
          uri = response.uri;
        }

        setPictureUri(uri);

        return uri;
      },

      uploadImage: async (onProgress?: (progress: number) => any) => {
        if (!pictureUri) {
          return false;
        }
        return await uploadImage(pictureUri, { onProgress });
      },
    },
  ];
}
