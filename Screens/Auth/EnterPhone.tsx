import React, { useState } from 'react';
import BackBtn from 'Core/Components/Buttons/Back';
import Header from 'Core/Layout/Header';
import PageLayout from 'Core/Layout/Page';
import { Alert, KeyboardAvoidingView, StyleSheet, View } from 'react-native';
import PhoneNumberInput from 'Core/Components/Inputs/Phone';
import NextBtn from 'Core/Components/Buttons/Next';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import AuthWithPhoneNumber from 'Core/Api/Auth/phoneNumber';
import { useTranslation } from 'react-i18next';
import { LogEvent } from 'Core/Libs/Amplitude';
import COLORS from 'Constants/Colors';

export default function EnterPhoneScreen() {
  const navigate = useNavigation<NativeStackNavigationProp<any>>();
  const { t } = useTranslation();

  const [nextEnable, setNextEnable] = useState<boolean>(false);
  const [phoneNumber, setPhoneNumber] = useState<string>('');

  const onValidPhoneNumber = (_phoneNumber: string) => {
    setNextEnable(!!_phoneNumber);
    setPhoneNumber(_phoneNumber);
  };

  const onPressNext = async () => {
    try {
      LogEvent('Submit Phone Number');
      await AuthWithPhoneNumber.signInWithPhoneNumber(phoneNumber);
      navigate.push('AuthValidatePhone');
    } catch (e: any) {
      // console.log(e);
      LogEvent('Phone Number Firebase Error', { errorMessage: e.message });
      Alert.alert(e.message);
    }
  };
  return (
    <PageLayout safeAreaBottom={true}>
      <Header
        title={t('createAccount')}
        subtitle={t('youllReceiveConfirmation')}
        iconLeft={
          <BackBtn
            onPress={() => {
              navigate.goBack();
            }}
            fill={COLORS.common.white_50}
          />
        }
      />
      <KeyboardAvoidingView
        keyboardVerticalOffset={15}
        style={styles.keyboardAvoidingView}
        behavior="padding">
        <View style={styles.viewContainer}>
          <PhoneNumberInput onChangeValidPhoneNumber={onValidPhoneNumber} />
        </View>
        <View style={styles.bottomButtonContainer}>
          <NextBtn disabled={!nextEnable} onPress={onPressNext} />
        </View>
      </KeyboardAvoidingView>
    </PageLayout>
  );
}

const styles = StyleSheet.create({
  keyboardAvoidingView: {
    flex: 1,
    // marginBottom: 15,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'blue',
  },
});
