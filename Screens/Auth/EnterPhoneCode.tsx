import React, { useState } from 'react';
import * as Sentry from '@sentry/react-native';
import BackBtn from 'Core/Components/Buttons/Back';
import Header from 'Core/Layout/Header';
import PageLayout from 'Core/Layout/Page';
import { Alert, KeyboardAvoidingView, StyleSheet, View } from 'react-native';

import NextBtn from 'Core/Components/Buttons/Next';
import { useNavigation } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import AuthWithPhoneNumber from 'Core/Api/Auth/phoneNumber';
import InputCode from 'Core/Components/Inputs/Code';
import { useAuthActions } from 'Core/hooks/useAuthActions';
import { useTranslation } from 'react-i18next';
import { LogEvent } from 'Core/Libs/Amplitude';
import COLORS from 'Constants/Colors';

export default function EnterPhoneCodeScreen() {
  const navigate = useNavigation<NativeStackNavigationProp<any>>();
  const { t } = useTranslation();
  const { authWithAccessToken } = useAuthActions();

  const [code, setCode] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const validateCodeAndNext = async (validationCode: string) => {
    try {
      setLoading(true);
      LogEvent('Submit Verify Code');
      const idToken = await AuthWithPhoneNumber.confirmWithCode(validationCode);
      const access_token = await AuthWithPhoneNumber.authWithIdToken(idToken);
      await authWithAccessToken(access_token);
    } catch (e: any) {
      console.log(e);
      LogEvent('Verify Code Firebase Error', { errorMessage: e.message });
      if (e.code === 'auth/invalid-verification-code') {
        Alert.alert(t('invalidVerification'));
      } else {
        Alert.alert(t('an_error_has_occured'));
        Sentry.captureException(e);
      }
      setLoading(false);
    }
  };

  const onPressNext = () => {
    validateCodeAndNext(code);
  };
  return (
    <PageLayout safeAreaBottom={true}>
      <Header
        title={t('enterValidationCode')}
        subtitle={t('checkYourMessages')}
        iconLeft={
          <BackBtn
            onPress={() => {
              navigate.goBack();
            }}
            fill={COLORS.common.white_50}
          />
        }
      />
      <KeyboardAvoidingView
        keyboardVerticalOffset={15}
        style={styles.keyboardAvoidingView}
        behavior="padding">
        <View style={styles.viewContainer}>
          <InputCode
            size={6}
            value={code}
            onChange={(newCode: string) => {
              setCode(newCode);
              if (newCode.length === 6) {
                validateCodeAndNext(newCode);
              }
            }}
          />
        </View>
        <View style={styles.bottomButtonContainer}>
          <NextBtn
            loading={loading}
            disabled={code.length !== 6}
            onPress={onPressNext}
          />
        </View>
      </KeyboardAvoidingView>
    </PageLayout>
  );
}

const styles = StyleSheet.create({
  keyboardAvoidingView: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
