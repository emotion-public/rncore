import React, { useState } from 'react';
import Header from 'Core/Layout/Header';
import PageLayout from 'Core/Layout/Page';
import { Alert, StyleSheet, View } from 'react-native';
// import ContactsPermissionsButton from 'Core/Components/AskPermissionsButtons/Contacts';
import NotificationsPermissionsButton from 'Core/Components/AskPermissionsButtons/Notifications';
import MicrophonePermissionsButton from 'Core/Components/AskPermissionsButtons/Microphone';
import ValidateBtn from 'Core/Components/Buttons/Validate';
import useOnboardingActions from 'Core/hooks/useOnboardingActions';
import { useTranslation } from 'react-i18next';
import { setUserProperties } from 'Core/Libs/Amplitude';

// type ALLOWED_PERMISSIONS = 'contacts' | 'microphone' | 'notifications';
type ALLOWED_PERMISSIONS = 'microphone' | 'notifications';
const ALLOWED_PERMISSIONS: Array<ALLOWED_PERMISSIONS> = [
  'notifications',
  'microphone',
];
export default function AllowPermissionsScreen({
  permissions = [],
}: {
  permissions?: Array<ALLOWED_PERMISSIONS>;
}) {
  const { t } = useTranslation();
  const { setPermissionScreenDone } = useOnboardingActions();

  type PermissionsGrantedType = {
    [key in ALLOWED_PERMISSIONS]?: boolean;
  };
  const [permissionsGranted, _setPermissionGranted] =
    useState<PermissionsGrantedType>({});

  const setPermissionGranted = (permissionGranted: PermissionsGrantedType) => {
    const newPermissionsGranted = {
      ...permissionsGranted,
      ...permissionGranted,
    };
    _setPermissionGranted(newPermissionsGranted);

    const allPermissions = permissions.filter(
      permission => ALLOWED_PERMISSIONS.indexOf(permission) !== -1,
    );
    if (
      allPermissions
        .map(permission => !!newPermissionsGranted[permission])
        .filter(isGranted => isGranted).length === allPermissions.length
    ) {
      setPermissionScreenDone();
    }
  };

  const allPermissions = permissions.filter(
    permission => ALLOWED_PERMISSIONS.indexOf(permission) !== -1,
  );
  const allHaveBeenAsked =
    Object.keys(permissionsGranted).length === allPermissions.length;

  return (
    <PageLayout safeAreaBottom={true}>
      <Header
        title={t('allowPermissions')}
        subtitle={t('toFullyEnjoy', { appName: 'voicebox' })}
      />

      <View style={styles.viewContainer}>
        {permissions.map(permission => {
          switch (permission) {
            // case 'contacts':
            //   return (
            //     <ContactsPermissionsButton
            //       key="contacts"
            //       style={styles.permissionButton}
            //       onGranted={() =>
            //         setPermissionGranted({
            //           ...permissionsGranted,
            //           contacts: true,
            //         })
            //       }
            //     />
            //   );
            case 'microphone':
              return (
                <MicrophonePermissionsButton
                  key="microphone"
                  style={styles.permissionButton}
                  onAskedResult={(granted: boolean) =>
                    setPermissionGranted({
                      ...permissionsGranted,
                      microphone: granted,
                    })
                  }
                />
              );
            case 'notifications':
              return (
                <NotificationsPermissionsButton
                  key="notifications"
                  style={styles.permissionButton}
                  onAskedResult={(granted: boolean) => {
                    setPermissionGranted({
                      ...permissionsGranted,
                      notifications: granted,
                    });
                    if (granted) {
                      setUserProperties({
                        'Push Notifications': true,
                      });
                    }
                  }}
                />
              );
            default:
              return null;
          }
        })}
      </View>
      <View style={styles.bottomButtonContainer}>
        {
          <ValidateBtn
            disabled={!allHaveBeenAsked}
            onPress={() => {
              if (!allHaveBeenAsked) {
                Alert.alert(t('alertAnswerPermissionsToContinue'));
              } else {
                setPermissionScreenDone();
              }
            }}
          />
        }
      </View>
    </PageLayout>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  permissionButton: {
    marginBottom: 17,
    width: '90%',
  },
});
