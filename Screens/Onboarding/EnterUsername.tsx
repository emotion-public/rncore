import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import Header from 'Core/Layout/Header';
import PageLayout from 'Core/Layout/Page';
import * as Animatable from 'react-native-animatable';
import FONTS from 'Constants/Fonts';
import { KeyboardAvoidingView, StyleSheet, Text, View } from 'react-native';
import NextBtn from 'Core/Components/Buttons/Next';
import TextInput from 'Core/Components/Inputs/Text';
import AtIcon from '../../Icons/at.svg';
import { useUpdateCurrentUserUsername } from 'Core/hooks/useCurrentUserActions';
import { useCurrentUser } from 'Core/hooks/useAuth';
import { useTranslation } from 'react-i18next';
import { LogEvent } from 'Core/Libs/Amplitude';
import { gql, useApolloClient } from '@apollo/client';
import { useDebounce } from 'use-debounce/lib';
import COLORS from 'Constants/Colors';

const CHECK_AVAILABILITY_QUERY = gql`
  query CHECK_USERNAME_AVAILIBILITY($username: String!) {
    checkUsernameAvailability(username: $username)
  }
`;

function IconAt() {
  return (
    <View style={styles.atIconContainer}>
      <AtIcon fill={COLORS.common.white_80} />
    </View>
  );
}

export default function EnterUsernameScreen({
  onEnd = () => {},
}: {
  onEnd: (arg?: any) => any;
}) {
  const apolloClient = useApolloClient();
  const { t } = useTranslation();
  const inputBounceable = useRef();
  const [usernameIsAvailable, setUsernameIsAvailable] =
    useState<{ available: boolean; value: string }>();
  const [usernameIsAvailableIsChecked, setUsernameIsAvailableIsChecked] =
    useState<boolean>(false);
  const [displayErrors, setDisplayErrors] = useState<boolean>(false);
  const [requestError, setRequestError] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const currentUser = useCurrentUser();
  const [updateUsername] = useUpdateCurrentUserUsername();

  const checkUsernameAvailable = useCallback(
    async (newUsernameValue: string): Promise<boolean> => {
      try {
        const isAvailableResult = await apolloClient.query({
          query: CHECK_AVAILABILITY_QUERY,
          fetchPolicy: 'network-only',

          variables: {
            username: newUsernameValue,
          },
        });
        return !!isAvailableResult.data?.checkUsernameAvailability;
      } catch (e: any) {
        console.log(JSON.stringify(e));
      }

      return false;
    },
    [apolloClient],
  );

  const { control, handleSubmit, watch, formState } = useForm({
    defaultValues: {
      username: currentUser?.username || '',
    },
    mode: 'onChange',
  });

  const { errors: formErrors } = formState;

  const clearError = () => {
    setDisplayErrors(false);
    setRequestError(null);
  };

  const onPressNext = async (data: any) => {
    try {
      setLoading(true);
      LogEvent('Submit Username');
      await updateUsername(data.username);
      onEnd();
    } catch (e: any) {
      if (e.code === 'DUPLICATE') {
        setRequestError(t('error_usernameAlreadyTaken'));
      } else {
        setRequestError(t('error_defaultMessage'));
      }
      setDisplayErrors(true);
      setLoading(false);
      (inputBounceable.current as any)?.swing(500);
    }
  };

  const usernameValue = watch('username');
  const [usernameValueDebounced] = useDebounce(usernameValue, 300);

  useEffect(() => {
    if (usernameValueDebounced.length < 3) {
      return;
    }
    (async () => {
      const isAvailable = await checkUsernameAvailable(usernameValueDebounced);
      if (isAvailable === undefined) {
        return true;
      }
      setUsernameIsAvailable({
        available: isAvailable,
        value: usernameValueDebounced,
      });
      setUsernameIsAvailableIsChecked(true);
      if (!isAvailable) {
        setDisplayErrors(true);
      }
    })();
  }, [usernameValueDebounced, checkUsernameAvailable]);

  useEffect(() => {
    setUsernameIsAvailableIsChecked(
      usernameIsAvailable?.value === usernameValue,
    );
  }, [usernameValue, usernameIsAvailable]);

  const errors = {
    ...formErrors,
  };
  let infoMessage = '';
  if (usernameIsAvailableIsChecked) {
    if (usernameIsAvailable?.available) {
      infoMessage = t('USER_PROFILE.info_usernameIsAvailable');
    } else {
      if (!errors.username?.message) {
        errors.username = {
          type: 'validate',
          message: t('USER_PROFILE.error_usernameAlreadyTaken'),
        };
      }
    }
  }

  return (
    <PageLayout safeAreaBottom={true}>
      <Header title={t('chooseUsername')} subtitle={t('howPeopleReachYou')} />
      <KeyboardAvoidingView
        keyboardVerticalOffset={15}
        style={styles.keyboardAvoidingView}
        behavior="padding">
        <View style={styles.viewContainer}>
          <View style={styles.textInputWithError}>
            <Controller
              name="username"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: t('error_usernameMandatory'),
                },
                minLength: {
                  value: 3,
                  message: t('error_usernameMinChars', { count: 3 }),
                },
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                // @ts-ignore
                <Animatable.View ref={inputBounceable}>
                  <TextInput
                    autoFocus
                    placeholder="zendaya"
                    keyboardType={'ascii-capable'}
                    keyboardAppearance="dark"
                    autoCorrect={false}
                    placeholderTextColor="rgba(255, 255, 255, 0.2)"
                    onChangeText={text => {
                      if (text === '') {
                        clearError();
                      }
                      onChange(text.toLowerCase().replace(/[^a-z0-9]/g, ''));
                    }}
                    autoCapitalize={'none'}
                    onBlur={onBlur}
                    value={value}
                    iconLeft={<IconAt />}
                    containerStyle={styles.textInputContainer}
                    style={styles.textInput}
                  />
                </Animatable.View>
              )}
            />
            {(displayErrors && (!!errors.username || !!requestError) && (
              <Text style={styles.textInputErrorMessage}>
                {errors.username?.message ||
                  requestError ||
                  t('error_defaultMessage')}
              </Text>
            )) ||
              (infoMessage !== '' && (
                <Text style={styles.textInputInfoMessage}>{infoMessage}</Text>
              ))}
          </View>
        </View>
        <View style={styles.bottomButtonContainer}>
          <NextBtn
            disabled={
              watch('username') === '' ||
              !!errors.username ||
              !usernameIsAvailableIsChecked
            }
            loading={loading}
            onPress={() => {
              setDisplayErrors(true);
              handleSubmit(onPressNext)();
            }}
          />
        </View>
      </KeyboardAvoidingView>
    </PageLayout>
  );
}

const styles = StyleSheet.create({
  keyboardAvoidingView: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  textInputWithError: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInputContainer: {
    width: 295,
  },
  textInput: {
    fontStyle: 'italic',
  },
  textInputErrorMessage: {
    position: 'absolute',
    top: 70,
    color: COLORS.common.red,
  },
  textInputInfoMessage: {
    position: 'absolute',
    fontFamily: FONTS.common.secondary,
    top: 70,
    color: COLORS.common.green,
  },

  atIconContainer: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
  },
});
