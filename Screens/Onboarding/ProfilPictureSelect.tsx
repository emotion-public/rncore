import React from 'react';
import Header from 'Core/Layout/Header';
import PageLayout from 'Core/Layout/Page';
import { StyleSheet, TouchableOpacity, View } from 'react-native';

import NextBtn from 'Core/Components/Buttons/Next';
import { useUploadNewCurrentUserProfilPic } from 'Core/hooks/useCurrentUserActions';
import LargeButton from 'Core/Components/Buttons/LargeButton';
import AvatarSquare from 'Core/Components/Avatar/Square';
import { useCurrentUser } from 'Core/hooks/useAuth';
import usePhotoPickerUploader from 'Core/PhotoPickerUploader/usePhotoPickerUploader';
import useOnboardingActions from 'Core/hooks/useOnboardingActions';
import IconButton from 'Components/Buttons/IconButton';
import PhotoLibraryIcon from 'Core/Icons/photo-library.svg';
import CameraIcon from 'Core/Icons/camera.svg';
import { useTranslation } from 'react-i18next';
import { LogEvent } from 'Core/Libs/Amplitude';
import SPACING from 'Constants/Spacing';
import { scale } from 'Core/Layout/ScaleHelpers';

export default function ProfilPictureSelectScreen() {
  const { t } = useTranslation();
  const currentUser = useCurrentUser();
  const [uploadAndUpdateProfilPicture] = useUploadNewCurrentUserProfilPic();
  const { setProfilPictureScreenDone } = useOnboardingActions();

  const [pictureUri, { launchCameraAsync, launchImageLibraryAsync }] =
    usePhotoPickerUploader();

  return (
    <PageLayout safeAreaBottom={true}>
      <Header title={t('selectProfilPic')} subtitle={t('howPeopleSeeYou')} />

      <View style={styles.viewContainer}>
        <View style={styles.pictureContainer}>
          <TouchableOpacity
            onPress={() => {
              launchImageLibraryAsync({ quality: 0.2 });
            }}>
            <AvatarSquare
              borderRadius={51}
              style={styles.avatar}
              imgUrl={pictureUri}
              letter={currentUser?.getFirstLetterName()}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.pictureLibraryButtonsContainer}>
          <LargeButton
            title={t('fromCamera')}
            variant="richWhite"
            onPress={() => {
              launchCameraAsync({ quality: 0.2 });
            }}
            iconLeft={
              <IconButton variant="blue">
                <CameraIcon fill={'white'} />
              </IconButton>
            }
          />
          <LargeButton
            title={t('fromLibrary')}
            variant="richWhite"
            onPress={() => {
              launchImageLibraryAsync({ quality: 0.2 });
            }}
            iconLeft={
              <IconButton variant="blue">
                <PhotoLibraryIcon fill={'white'} />
              </IconButton>
            }
            style={styles.fromLibraryButton}
          />
        </View>
      </View>
      <View style={styles.bottomButtonContainer}>
        <NextBtn
          variant="gray"
          disabled={!pictureUri}
          onPress={async () => {
            if (!pictureUri) {
              setProfilPictureScreenDone();
              return false;
            }
            setProfilPictureScreenDone();
            LogEvent('Start Upload Avatar');
            try {
              await uploadAndUpdateProfilPicture(pictureUri);
              LogEvent('Upload Avatar Success');
            } catch (e: any) {
              LogEvent('Upload Avatar Error');
            }
          }}
        />
      </View>
    </PageLayout>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  pictureContainer: {
    flex: 1,
    justifyContent: 'center',
  },

  fromLibraryButton: { marginTop: 12 },

  avatar: {
    width: scale(140),
    height: scale(140),
  },

  pictureLibraryButtonsContainer: {
    width: SPACING.deviceWidth - 30,
    marginBottom: 20,
    // height: 158,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});
