import PlayPauseBtn from 'Components/Player/Components/PlayPauseBtn';
import {
  usePlayer,
  usePlayerPositionState,
  usePlayerState,
} from 'Core/AudioPlayer/Player';
import LargeButton from 'Core/Components/Buttons/LargeButton';
import React from 'react';
import { StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';

export default function AudioPlayerTest({
  style,
}: {
  style?: StyleProp<ViewStyle>;
}) {
  const player = usePlayer();
  const playerState = usePlayerState();
  const playerPositionState = usePlayerPositionState();

  const onPressPlay = () => {
    player.play({
      title: 'Test',
      url: 'https://voicebox-paris.s3.eu-west-3.amazonaws.com/0d5ac705/4c16/0d5ac705-4c16-46ab-8d3a-ae907caa36b1.aac',
      soundId: 'test111',
    });
  };

  const onPressPause = () => {
    player.pause();
  };

  return (
    <View style={[styles.container, style]}>
      <LargeButton
        title={playerState.isPlaying ? 'pause' : 'play'}
        onPress={playerState.isPlaying ? onPressPause : onPressPlay}
      />

      <PlayPauseBtn
        isPlaying={playerState.isPlaying}
        onPress={playerState.isPlaying ? onPressPause : onPressPlay}
      />

      <Text style={styles.text}>{playerPositionState.positionMillis}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '50%',
    backgroundColor: 'black',

    justifyContent: 'space-evenly',
  },

  text: {
    color: 'white',
  },
});
