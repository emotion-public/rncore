import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { REHYDRATE } from 'redux-persist';
import User from 'Models/User';

interface AuthState {
  access_token: string | null;
  current_user: User | null;
  isLoading: boolean;
}

const initialState: AuthState = {
  access_token: null,
  current_user: null,
  isLoading: true,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setIsLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload;
    },
    setCurrentUser: (state, action: PayloadAction<User>) => {
      state.current_user = action.payload;
    },
    setAuth: (
      state,
      action: PayloadAction<{
        access_token: string | null;
        current_user: User | null;
      }>,
    ) => {
      state.access_token = action.payload.access_token;
      state.current_user = action.payload.current_user;
    },

    clearAllData: state => {
      state.access_token = null;
      state.current_user = null;
    },
  },
  extraReducers: builder => {
    builder.addCase(REHYDRATE, (state, action: any) => {
      if (action.payload?.auth) {
        state = action.payload.auth;
      }
      state.isLoading = false;
    });
  },
});

export const { setAuth, setIsLoading, setCurrentUser, clearAllData } =
  authSlice.actions;
export default authSlice.reducer;
