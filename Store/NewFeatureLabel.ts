import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { REHYDRATE } from 'redux-persist';
import featureList from 'Core/featureList';

type NewFeatureLabelState = {
  [key in keyof typeof featureList]?: boolean;
};

const initialState: NewFeatureLabelState = {};

export const newFeatureLabelSlice = createSlice({
  name: 'newFeatureLabel',
  initialState,
  reducers: {
    setNewFeatureLabelViewed: (
      state,
      action: PayloadAction<{ featureName: keyof typeof featureList }>,
    ) => {
      state[action.payload.featureName] = true;
    },
  },
  extraReducers: builder => {
    builder.addCase(REHYDRATE, (state, action: any) => {
      if (action.payload?.newFeatureLabel) {
        state = action.payload.newFeatureLabel;
      }
    });
  },
});

export const { setNewFeatureLabelViewed } = newFeatureLabelSlice.actions;
export default newFeatureLabelSlice.reducer;
