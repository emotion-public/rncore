import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { REHYDRATE } from 'redux-persist';

interface OnboardingState {
  hasDoneUsername?: boolean;
  hasDonePermissions?: boolean;
  hasDoneProfilPicture?: boolean;
  hasDoneWhyUse?: boolean;
}

const initialState: OnboardingState = {
  hasDoneUsername: false,
  hasDonePermissions: false,
  hasDoneProfilPicture: false,
  hasDoneWhyUse: false,
};

export const onboardingSlice = createSlice({
  name: 'onboarding',
  initialState,
  reducers: {
    setOnboarding: (state, action: PayloadAction<OnboardingState>) => {
      state.hasDoneProfilPicture =
        action.payload.hasDoneProfilPicture ?? state.hasDoneProfilPicture;
      state.hasDoneUsername =
        action.payload.hasDoneUsername ?? state.hasDoneUsername;
      state.hasDonePermissions =
        action.payload.hasDonePermissions ?? state.hasDonePermissions;
      state.hasDoneWhyUse = action.payload.hasDoneWhyUse ?? state.hasDoneWhyUse;
    },
    clearOnboarding: state => {
      state.hasDoneProfilPicture = false;
      state.hasDoneUsername = false;
      state.hasDonePermissions = false;
      state.hasDoneWhyUse = false;
    },
  },
  extraReducers: builder => {
    builder.addCase(REHYDRATE, (state, action: any) => {
      if (action.payload?.onboarding) {
        state = action.payload.onboarding;
      }
    });
  },
});

export const { setOnboarding, clearOnboarding } = onboardingSlice.actions;
export default onboardingSlice.reducer;
