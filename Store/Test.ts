import { createSlice } from '@reduxjs/toolkit';

interface TestState {
  counter: number;
}

const initialState: TestState = {
  counter: 0,
};

export const slice = createSlice({
  name: 'test',
  initialState,
  reducers: {
    addOne: state => {
      state.counter = state.counter + 1;
    },
  },
});

export const { addOne } = slice.actions;
export default slice.reducer;
