import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface ThemeState {
  themeName: string;
}

const initialState: ThemeState = {
  themeName: 'default',
};

export const slice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    setThemeName: (state, action: PayloadAction<string>) => {
      state.themeName = action.payload;
    },
  },
});

export const { setThemeName } = slice.actions;
export default slice.reducer;
