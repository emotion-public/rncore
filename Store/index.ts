import {
  combineReducers,
  configureStore as baseConfigureStore,
} from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import persistStore from 'redux-persist/es/persistStore';
import authReducer from './Auth';
import onboardingReducer from './Onboarding';
import newFeatureLabelReducer from './NewFeatureLabel';
import testReducer from './Test';
import themeReducer from './Theme';

export default function configureStore(config?: {
  reducers?: { [key: string]: any };
  whitelist?: Array<string>;
}) {
  const { reducers, whitelist } = config || {};
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: [
      'auth',
      'onboarding',
      'newFeatureLabel',
      'test',
      ...(whitelist || []),
    ],
  };
  const persistedReducer = persistReducer(
    persistConfig,
    combineReducers({
      auth: authReducer,
      onboarding: onboardingReducer,
      newFeatureLabel: newFeatureLabelReducer,
      test: testReducer,
      theme: themeReducer,
      ...(reducers || {}),
    }),
  );

  const store = baseConfigureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware({
        serializableCheck: {
          ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
      }),
  });

  const persistor = persistStore(store);

  return { store, persistor };
}
