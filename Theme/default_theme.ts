import { THEMES } from 'Core/Theme';

const themes: THEMES = {
  default: {
    app_background_color: 'yellow',
  },
  light: {},
  dark: {},
};

export default themes;
