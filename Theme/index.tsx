import React, { useContext } from 'react';
import { useAppSelector } from 'Core/Store/hooks';
import { useAppDispatch } from 'Core/Store/hooks';
import { setThemeName } from 'Core/Store/Theme';
import { RootState } from 'Store';
import defaultTheme from './default_theme';
import appTheme from 'appTheme';

export type THEME = {
  app_background_color?: string;
  btn_icon_color?: string;
};

export type THEMES = { [key: string]: THEME };

let themes: { [key: string]: THEME } = {
  default: defaultTheme.default,
  ...defaultTheme,
  ...appTheme,
};

let ThemeContext: React.Context<THEME>;

export function ThemeProvider({ children }: { children: React.ReactNode }) {
  if (!ThemeContext) {
    ThemeContext = React.createContext<THEME>(defaultTheme.light);
  }
  const themeName = useAppSelector(
    (state: RootState) => state.theme.themeName || 'default',
  );

  const theme = themes[themeName] || themes.default;
  // console.log('init with theme', themeName, theme);

  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
}

export function useTheme() {
  return useContext(ThemeContext);
}

export function useSetThemeName() {
  const dispatch = useAppDispatch();

  return (themeName: string) => {
    dispatch(setThemeName(themeName));
  };
}
