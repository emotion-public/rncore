export type CONFIG = {
  onesignal?: {
    appId: string;
  };
  amplitude?: {
    apiKeyDev: string;
    apiKeyProd: string;
  };
};

let _CONFIG: CONFIG = {};

export function getCoreConfig() {
  return {
    ..._CONFIG,
  };
}

export function setCoreConfig(_config: CONFIG) {
  _CONFIG = _config;
}
