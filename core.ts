import { useState } from 'react';

export default function useTest(): [number, () => void] {
  const [test, setTest] = useState<number>(0);

  return [
    test,
    () => {
      setTest(test + 2);
    },
  ];
}
