export default {
  newProfileEdit: {
    endDate: '2022-07-28',
  },

  newCurrentUserProfile: {
    endDate: '2022-07-28',
  },

  introVoiceLimit: {
    endDate: '2022-07-08',
  },
  introVoiceLimitEdit: {
    endDate: '2022-07-08',
  },
  introVoiceLimitEditModal: {
    endDate: '2022-07-08',
  },

  shareVoice: {
    endDate: '2022-07-08',
  },
};
