import { useNavigationState } from '@react-navigation/native';

function getCurrentRoute(route?: any): any {
  if (!route) {
    return route;
  }

  if (route.state) {
    return getCurrentRoute(route.state);
  }

  if (route.index !== undefined && route.routes?.length > 0) {
    return getCurrentRoute(route.routes[route.index]);
  }
  return route;
}

export default function useCurrentRoute() {
  const route = useNavigationState(state => {
    if (!state) {
      return null;
    }
    return state.routes[state.index];
  });

  let currentRoute:
    | { name: string; params: any; state?: any }
    | typeof route
    | Partial<typeof route>
    | null = getCurrentRoute(route);

  return currentRoute;
}
