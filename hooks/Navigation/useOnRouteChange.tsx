import usePrevious from '@react-hook/previous';
import useCurrentRoute from 'Core/hooks/Navigation/useCurrentRoute';
import { useEffect } from 'react';

export default function useOnRouteChange(
  onRouteChangeCallback: (routname: string) => any,
) {
  const currentRoute = useCurrentRoute();
  const currentRouteName = currentRoute?.name;
  const previousCurrentRouteName = usePrevious(currentRouteName);

  useEffect(() => {
    if (currentRouteName && currentRouteName !== previousCurrentRouteName) {
      onRouteChangeCallback(currentRouteName);
    }
  }, [currentRouteName, previousCurrentRouteName, onRouteChangeCallback]);
}
