import { useEffect, useRef } from 'react';
import { AppState, AppStateStatus } from 'react-native';

export default function useAppStateChanged(
  onAppChange: ({
    appState,
    previousAppState,
    timeSinceLastStateMs,
  }: {
    appState: AppStateStatus;
    previousAppState: AppStateStatus;
    timeSinceLastStateMs: number;
  }) => any,
) {
  const appState = useRef(AppState.currentState);
  const previousAppState = useRef<AppStateStatus | null>(null);
  const timeOfStateChange = useRef<number>(new Date().getTime());

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      // if (
      //   appState.current.match(/inactive|background/) &&
      //   nextAppState === 'active'
      // ) {
      //   console.log('App has come to the foreground!');
      // }

      const lastTimeOfStateChange: number = timeOfStateChange.current;
      previousAppState.current = appState.current;
      appState.current = nextAppState;
      timeOfStateChange.current = new Date().getTime();

      onAppChange({
        appState: appState.current,
        previousAppState: previousAppState.current,
        timeSinceLastStateMs: timeOfStateChange.current - lastTimeOfStateChange,
      });
    });

    return () => {
      subscription.remove();
    };
  }, [onAppChange]);
}
