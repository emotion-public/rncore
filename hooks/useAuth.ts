import { gql } from '@apollo/client';
import { useAppSelector } from 'Core/Store/hooks';
import User from 'Models/User';
import { RootState } from 'Store';

const authSelector = (state: RootState) => state.auth || {};

export const ME_FIELDS = `

  id
  uuid
  username
  bio
  shareUrl
  displayName
  onBoarding
  email
  phone
  avatar
  avatarUrl

  link
  color
  location
  introVoiceLimit
`;

export const GET_ME = gql`
  query getMe {
    me {
      ${ME_FIELDS}
    }
  }
`;

export const useAuth = () => {
  const auth = useAppSelector(authSelector);

  const current_user_data = auth.current_user;
  let current_user = current_user_data;
  if (current_user_data) {
    current_user = new User();
    current_user.setRawData(current_user_data);
  }

  return {
    isLogged: !!auth.access_token,
    isLoading: auth.isLoading,
    access_token: auth.access_token,
    current_user: current_user,
  };
};

export const useCurrentUser = () => {
  const auth = useAuth();

  return auth.current_user || null;
};
