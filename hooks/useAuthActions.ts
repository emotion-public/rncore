import OneSignal from 'react-native-onesignal';
import { useAppDispatch } from '../Store/hooks';
import {
  setIsLoading,
  setAuth,
  setCurrentUser,
  clearAllData as clearAuthData,
} from '../Store/Auth';
import { GET_ME } from './useAuth';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { AppDispatch, RootState } from 'Store';
import { createApolloClient } from 'Core/Api/ApolloGraphQl';
import { getCoreConfig } from 'Core/config';
import {
  setUserId as setAmplitudeUserId,
  setUserProperties,
} from 'Core/Libs/Amplitude';
import User from 'Models/User';
import { clearOnboarding } from 'Core/Store/Onboarding';

let _fetchAllUserData =
  () => async (_dispatch: AppDispatch, _getState: () => RootState) => {
    try {
      console.log('Fetch nothing');
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  };

export function setFetchAllUserData(
  fetchAllUserData: () => (
    dispatch: AppDispatch,
    getState: () => RootState,
  ) => Promise<any>,
) {
  _fetchAllUserData = fetchAllUserData;
}

export const fetchAllUserData =
  () => async (dispatch: AppDispatch, _getState: () => RootState) => {
    try {
      // @ts-ignore
      await dispatch(_fetchAllUserData());
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  };

export const refreshMeData =
  () => async (dispatch: AppDispatch, getState: () => RootState) => {
    try {
      const state = getState();
      if (!state.auth?.access_token) {
        return false;
      }
      const { access_token } = state.auth;
      const gqlClient: ApolloClient<NormalizedCacheObject> =
        await createApolloClient({
          access_token,
        } as any);
      const rs = await gqlClient.query({
        query: GET_ME,
      });

      const current_user = rs.data.me;
      dispatch(setCurrentUser(current_user));
      // @ts-ignore
      await dispatch(_fetchAllUserData());
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  };

const authWithAccessToken =
  (access_token: string) => async (dispatch: AppDispatch) => {
    try {
      const gqlClient: ApolloClient<NormalizedCacheObject> =
        await createApolloClient({
          access_token,
        } as any);
      const rs = await gqlClient.query({
        query: GET_ME,
        fetchPolicy: 'network-only',
      });

      if (!rs.data || !rs.data.me) {
        console.log('ERROR', JSON.stringify(rs.error));
        throw new Error('Une erreur est survenue');
      }
      const current_user = {
        ...rs.data.me,
      };
      const authData = {
        access_token,
        current_user,
      };

      const currentUser = new User();
      currentUser.setRawData(current_user);

      const config = getCoreConfig();
      if (config.onesignal) {
        OneSignal.setExternalUserId(current_user.uuid);
      }

      if (config.amplitude && currentUser.uuid) {
        setUserProperties({
          userUUID: currentUser.uuid,
        });
        setAmplitudeUserId(currentUser.uuid);
      }

      dispatch(setAuth(authData));
      // @ts-ignore
      await dispatch(_fetchAllUserData());
    } catch (e: any) {
      console.log(e.message);
      console.log('ERROR GQL', JSON.stringify(e));
      throw e;
    }
  };

const logout = () => async (dispatch: AppDispatch) => {
  dispatch(setIsLoading(true));
  dispatch(clearAuthData());
  dispatch(clearOnboarding());
  dispatch(setIsLoading(false));
};

export const useAuthActions = () => {
  const dispatch = useAppDispatch();

  return {
    refreshMeData: async () => {
      // @ts-ignore
      return await dispatch(refreshMeData());
    },

    fetchAllUserData: async () => {
      // @ts-ignore
      return await dispatch(fetchAllUserData());
    },

    authWithAccessToken: async (access_token: string) =>
      dispatch(authWithAccessToken(access_token)),

    logout: async () => {
      dispatch(logout());
    },
  };
};
