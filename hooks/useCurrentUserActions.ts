import { gql, useMutation } from '@apollo/client';
import { useAppDispatch } from '../Store/hooks';
import { setCurrentUser } from '../Store/Auth';
import { ME_FIELDS, useCurrentUser } from 'Core/hooks/useAuth';
import uploadFile from 'Core/Api/Upload';
import ErrorDuplicate from 'Core/Errors/ErrorDuplicate';
import { Alert } from 'react-native';
import { useTranslation } from 'react-i18next';

const UPDATE_ME = gql`
    mutation UpdateMe($user: UserUpdate!) {
        updateMe(user: $user) {
          ${ME_FIELDS}
        }
    }
`;

const UPDATE_ME_USERNAME = gql`
    mutation UpdateMeUsername($username: String!) {
        updateUsername(username: $username) {
          ${ME_FIELDS}
        }
    }
`;

export const useUpdateCurrentUser = (): [
  (userData: any) => Promise<any>,
  any,
] => {
  const [updateMe, { data, loading, error }] = useMutation(UPDATE_ME);
  const dispatch = useAppDispatch();

  return [
    async (userData: any) => {
      try {
        const result = await updateMe({
          variables: {
            user: userData,
          },
        });

        if (result.data?.updateMe) {
          dispatch(setCurrentUser(result.data.updateMe as any));
        }
      } catch (e) {
        console.log(JSON.stringify(e));
        throw e;
      }
    },
    { data, loading, error },
  ];
};

export const useUpdateCurrentUserUsername = (): [
  (username: string) => Promise<any>,
  any,
] => {
  const [updateMeUsername, { data, loading, error }] =
    useMutation(UPDATE_ME_USERNAME);
  const dispatch = useAppDispatch();

  return [
    async (username: string) => {
      try {
        const result = await updateMeUsername({
          variables: {
            username,
          },
        });

        if (result.data?.updateUsername) {
          dispatch(setCurrentUser(result.data.updateUsername as any));
        }
      } catch (e: any) {
        if (e.graphQLErrors) {
          if (
            e.graphQLErrors.filter(
              (graphQLError: any) =>
                graphQLError.extensions.code === 'DUPLICATE',
            ).length > 0
          ) {
            throw new ErrorDuplicate('username');
          }
        }
        throw e;
      }
    },
    { data, loading, error },
  ];
};

export const useUploadNewCurrentUserProfilPic = (): [
  (userData: any) => Promise<any>,
  any,
] => {
  const { t } = useTranslation();
  const [updateMe, { data, loading, error }] = useMutation(UPDATE_ME);
  const dispatch = useAppDispatch();
  const currentUser = useCurrentUser();

  return [
    async (pictureUri: any) => {
      const oldProfilPicture = currentUser?.avatarUrl;
      try {
        dispatch(
          setCurrentUser({
            ...currentUser,
            avatarUrl: pictureUri,
          } as any),
        );
        const imageUri = await uploadFile(pictureUri);
        if (!imageUri) {
          throw new Error(t('an_error_has_occured'));
        }
        const result = await updateMe({
          variables: {
            user: {
              avatar: imageUri,
            },
          },
        });

        if (result.data?.updateMe) {
          dispatch(setCurrentUser(result.data.updateMe as any));
        }
      } catch (e: any) {
        dispatch(
          setCurrentUser({
            ...currentUser,
            avatarUrl: oldProfilPicture,
          } as any),
        );
        console.log(JSON.stringify(e));
        Alert.alert(e.message);
      }
    },
    { data, loading, error },
  ];
};
