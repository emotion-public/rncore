import { activateKeepAwake } from 'expo-keep-awake';
import { useEffect } from 'react';

export default function useDevKeepAwake() {
  useEffect(() => {
    if (__DEV__) {
      activateKeepAwake();
    }
  }, []);
}
