import featureList from 'Core/featureList';
import { useAppDispatch } from 'Core/Store/hooks';
import { setNewFeatureLabelViewed } from 'Core/Store/NewFeatureLabel';

export default function useNewFeatureLabelActions() {
  const dispatch = useAppDispatch();

  return {
    setNewFeatureLabelViewed: (featureName: keyof typeof featureList) => {
      dispatch(
        setNewFeatureLabelViewed({
          featureName,
        }),
      );
    },
  };
}
