import { useAppSelector } from 'Core/Store/hooks';
import { RootState } from 'Store';

export default function useNewFeatureLabelState() {
  const newFeatureLabel = useAppSelector(
    (state: RootState) => state.newFeatureLabel || {},
  );
  return newFeatureLabel;
}
