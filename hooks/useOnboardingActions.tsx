import { useAppDispatch } from 'Core/Store/hooks';
import { setOnboarding } from 'Core/Store/Onboarding';

export default function useOnboardingActions() {
  const dispatch = useAppDispatch();

  return {
    setProfilPictureScreenDone: () => {
      dispatch(
        setOnboarding({
          hasDoneProfilPicture: true,
        }),
      );
    },
    setPermissionScreenDone: () => {
      dispatch(
        setOnboarding({
          hasDonePermissions: true,
        }),
      );
    },
    setWhyUseScreenDone: () => {
      dispatch(
        setOnboarding({
          hasDoneWhyUse: true,
        }),
      );
    },
  };
}
