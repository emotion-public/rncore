import { useAppSelector } from 'Core/Store/hooks';
import { RootState } from 'Store';

export default function useOnboardingState() {
  const onboarding = useAppSelector(
    (state: RootState) => state.onboarding || {},
  );

  return {
    hasDoneProfilPicture: onboarding.hasDoneProfilPicture,
    hasDoneUsername: onboarding.hasDoneUsername,
    hasDonePermissions: onboarding.hasDonePermissions,
    hasDoneWhyUse: onboarding.hasDoneWhyUse,
  };
}
