import { useNavigationContainerRef } from '@react-navigation/native';
import { LogEvent } from 'Core/Libs/Amplitude';
import { useRef } from 'react';

export default function useReactNavigationScreenTracking() {
  const navigationRef = useNavigationContainerRef();
  const routeNameRef = useRef<string>();

  const onReady = () => {
    routeNameRef.current = navigationRef.getCurrentRoute()?.name;
  };

  const onStateChange = async () => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.getCurrentRoute()?.name;

    if (previousRouteName !== currentRouteName) {
      LogEvent(
        'Navigate to Screen : ' + currentRouteName,
        previousRouteName ? { previous: previousRouteName } : undefined,
      );
    }

    routeNameRef.current = currentRouteName;
  };

  return {
    navigationRef,
    routeNameRef,
    onReady,
    onStateChange,
  };
}
