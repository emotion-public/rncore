import dateFormat from 'dateformat';
import useAppStateChanged from 'Core/hooks/useAppState';
import { LogEvent, setUserProperties } from 'Core/Libs/Amplitude';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect } from 'react';

const TIME_BETWEEN_SESSIONS = 10 * 60 * 1000;

function getCurrentDay() {
  return dateFormat(new Date(), 'yyyy-mm-dd');
}

async function incrementStoredValue(key: string) {
  let dailySessionCount = parseInt(
    (await AsyncStorage.getItem(key)) || '0',
    10,
  );
  dailySessionCount = dailySessionCount + 1;
  await AsyncStorage.setItem(key, String(dailySessionCount));
}
async function getStoredValue(key: string) {
  return parseInt((await AsyncStorage.getItem(key)) || '0', 10);
}

async function incrementDailySession() {
  await incrementStoredValue('daily_session_count');
  const dailySessionCount = await getStoredValue('daily_session_count');
  setUserProperties({
    'Count of Total Daily Openings': dailySessionCount,
  });
}

async function incrementTotalSessions() {
  const currentDay = getCurrentDay();
  await incrementStoredValue('total_session_count');
  const totalSessionCount = await getStoredValue('total_session_count');
  setUserProperties({
    'Count of Sessions': totalSessionCount,
    'Last Session Date': currentDay,
  });
}

export default function useSessionLogger() {
  useAppStateChanged(({ appState, timeSinceLastStateMs }) => {
    if (appState === 'active') {
      if (timeSinceLastStateMs > TIME_BETWEEN_SESSIONS) {
        LogEvent('New Session');
        incrementTotalSessions();
      }
    }
  });

  useEffect(() => {
    const currentDay = getCurrentDay();
    LogEvent('New Session');
    incrementTotalSessions();
    (async () => {
      const lastSessionDate = await AsyncStorage.getItem('last_session_date');
      AsyncStorage.setItem('last_session_date', currentDay);
      if (lastSessionDate !== currentDay) {
        incrementDailySession();
      }
    })();
  }, []);
}
