import { useAppSelector } from 'Core/Store/hooks';
import { RootState } from 'Store';

export default function useCounter() {
  const test = useAppSelector((state: RootState) => state.test || {});

  return test.counter || 0;
}
