import { useAppDispatch } from 'Core/Store/hooks';
import { addOne } from 'Core/Store/Test';

export default function useTestActions() {
  const dispatch = useAppDispatch();

  return () => {
    dispatch(addOne());
  };
}
