import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { AppDispatch, RootState } from 'Store';
import {
  createApolloClient,
  getApolloSharedClient,
} from 'Core/Api/ApolloGraphQl';

export default function withApolloClientForActions(
  callbackFunction: (
    dispatch: AppDispatch,
    getState: () => RootState,
    gqlClient: ApolloClient<NormalizedCacheObject> | null,
  ) => Promise<any>,
) {
  return async (dispatch: AppDispatch, getState: () => RootState) => {
    let gqlClient: ApolloClient<NormalizedCacheObject> | null = null;

    const state = getState();
    if (state.auth?.access_token) {
      const { access_token } = state.auth;
      gqlClient = await createApolloClient({
        access_token,
      } as any);
    }
    return await callbackFunction(dispatch, getState, gqlClient);
  };
}

export function withApolloSharedClientForActions(
  callbackFunction: (
    dispatch: AppDispatch,
    getState: () => RootState,
    gqlClient: ApolloClient<NormalizedCacheObject> | null,
  ) => Promise<any>,
) {
  return async (dispatch: AppDispatch, getState: () => RootState) => {
    let gqlClient: ApolloClient<NormalizedCacheObject> | null =
      getApolloSharedClient();
    return await callbackFunction(dispatch, getState, gqlClient);
  };
}
